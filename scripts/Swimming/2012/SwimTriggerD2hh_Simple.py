from Configurables import Swimming
from Gaudi.Configuration import *

d0loc = "/Event/CharmToBeSwum/Phys/D2hhPromptD2{hh}D2hhHlt2TOS"
d0barloc = "/Event/CharmToBeSwum/Phys/SelConjugateD2hhPromptD2{hh}"
dstloc = '/Event/CharmToBeSwum/Phys/D2hhPromptDst2D2{hh}Line'
tmp = [
    (d0loc, 'PiPi', 'PiPi'), 
    (d0barloc, 'PiPi', 'PiPi'), 
    (d0loc, 'KK', 'KK'), 
    (d0barloc, 'KK', 'KK'), 
    (d0loc, 'KPi', 'RS'),
    ('/Event/CharmToBeSwum/Phys/D2hhPromptD0WS', '', 'WS')
    ]
hhes = [ 'PiPi', 'KK', 'KPi' ]
p2tpprefix = Swimming().getProp('SwimmingPrefix')

Swimming().Debug = False
Swimming().DataType = '2012'
Swimming().EvtMax = -1
Swimming().Simulation = False
Swimming().Persistency = 'ROOT'
Swimming().InputType = 'DST'
Swimming().SwimStripping = False
Swimming().Hlt1Triggers = ["Hlt1TrackAllL0Decision","Hlt1TrackPhotonDecision"]
Swimming().Hlt2Triggers = ["Hlt2CharmHadD02HH_D02{hh}{wm}Decision".format(hh=hh,wm=wm) for wm in ['','WideMass'] for hh in hhes]
Swimming().OffCands = dict( (loc.format(hh=hh), p2tpprefix) for loc, hh, dsthh in tmp )
Swimming().MuDSTCands = [ dstloc.format(hh=hh) for hh in ['RS','WS','KK','PiPi'] ]
Swimming().SkipEventIfNoMuDSTCandFound = True
Swimming().SkipEventIfNoMuDSTCandsAnywhere = False
Swimming().TransformName = '2012_WithBeamSpotFilter_NoRecoLines'
Swimming().SelectMethod = 'all'
Swimming().OutputType = 'DST'
Swimming().UseFileStager = False

# The custom event loop
from Gaudi.Configuration import setCustomMainLoop
from Swimming.EventLoop import SwimmingEventLoop
setCustomMainLoop(SwimmingEventLoop)
