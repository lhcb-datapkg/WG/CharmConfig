from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer, RawEventJuggler, RawEventFormatConf
from Configurables import LHCbApp
LHCbApp()
#LHCbApp().EvtMax = 10

MySeq = GaudiSequencer("JuggleForSwimming")

RawEventFormatConf().forceLoad()
myLocs = RawEventFormatConf().Locations
myNames= RawEventFormatConf().RecoDict
newName = "Custom"
oldName = "Moore"
myLocs[newName] = myLocs[myNames[oldName]]
for k in myLocs[newName].keys():
  if str(k).startswith('Hlt'):
    myLocs[newName][k] = 'PrevTrig/RawEvent'
myNames[newName] = newName

RawEventJuggler().Input = "Stripping20"
RawEventJuggler().Output = newName
RawEventJuggler().Sequencer = MySeq
RawEventJuggler().KillInputBanksAfter = "Hlt*"
RawEventJuggler().KillExtraNodes = True
RawEventJuggler().KillExtraDirectories = True
ApplicationMgr().TopAlg = [MySeq]
