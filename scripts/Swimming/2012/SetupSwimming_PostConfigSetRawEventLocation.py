from Gaudi.Configuration import *

def fixeverything():
  from Gaudi.Configuration import allConfigurables
  for name,config in allConfigurables.iteritems():
    for slot in config.__slots__:
      if hasattr(config, slot):
        attr = getattr(config, slot)
        if isinstance(attr, basestring):
          if "DAQ" in attr:
            print config
            setattr(config, slot, attr.replace("DAQ", "Trigger"))
            print config

appendPostConfigAction(fixeverything)
