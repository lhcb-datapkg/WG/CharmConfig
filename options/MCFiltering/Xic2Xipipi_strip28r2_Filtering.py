"""
Stripping Filtering file for Xic-> Xi pi pi 2018
@author Sergio Jaimes Elles
@date   2023-01-19
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"


#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping28r2'

# Get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
# Get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("Hc2V2H_Xic2Xipipi.StripTrig")
MyLines = [ 'StrippingHc2V2H_Xic2XipipiDDDLine',
            'StrippingHc2V2H_Xic2XipipiDDLLine',
            'StrippingHc2V2H_Xic2XipipiLLLLine']

linesToAdd = []
for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                print("APPENDING LINE {0}".format(line.name()))
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only filtered events

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf)

SelDSTWriterElements = {
    'default': stripMicroDSTElements(pack=enablePacking,isMC=True)
}

SelDSTWriterConf = {
    'default': stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
}

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
)

# Add stripping TCK 
# TCK = 0xVVVVSSSS, where VVVV is the DaVinci version, and SSSS is the Stripping number
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x4261291)


# --- Trigger filtering ---
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_DECISION_PHYSICS"
)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60