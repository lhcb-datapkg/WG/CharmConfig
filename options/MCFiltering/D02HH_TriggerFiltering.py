"""
Option file to filter simulated Turbo data based on the HLT2 trigger decision
(for Run 2). Relevant lines are: 
  - Hlt2CharmHadDstp2D0Pip_D02{xx}Turbo
  - Hlt2CharmHadDstp2D0Pip_D02{xx}_LTUNBTurbo
where {xx} = KmKp, PimPip, KmPip, KpPim
@author Tommaso Pajero <tommaso.pajero@cern.ch>
@date 2019-12-17
"""

from Configurables import GaudiSequencer

#trigger filter
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code = "  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1CalibTracking.*Decision')",
    HLT2_Code = "  HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KmKpTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02PimPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KpPimTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurbo.*Decision' )"
    )

# DaVinci Configuration
from Gaudi.Configuration import *
from Configurables import DaVinci

output_name = "D02HH"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().RootInTES = '/Event/Turbo'


# configure the copy-stream algorithm
# =============================================================================

oname = output_name + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh = IOHelper()
copy = ioh.outputAlgs(oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )


# Code to test the filter
# =============================================================================

#DaVinci().Simulation = True
#DaVinci().DataType = '2017'
#DaVinci().InputType = 'DST'
#DaVinci().EvtMax = -1
#
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
#
#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles([
#
#    # EventType 11774004
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099093/0000/00099093_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [1, 10, 11, 16, 18, 20, 25, 26, 27]
#
#    # EventType 12365401
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099091/0000/00099091_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [6, 7, 8, 9, 10, 11, 13, 16, 18]
#
#    # EventType 27163100
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099089/0000/00099089_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [7, 10, 13, 14, 17, 18, 20, 23, 25]
#        for n in [7]
#    ],
#    clear=True)
