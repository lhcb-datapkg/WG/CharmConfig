"""
Turbo Filtering script for Monte Carlo for Hlt2CharmHadD02XXXXTurbo
@author Maurizio Martinelli
@date   2018-08-16
"""

from Configurables import GaudiSequencer

# line names
lnames = ['KmPimPipPip','KpPimPimPip',
          'PimPimPipPip','KmKpPimPip',
          'KmKmKpPip','KmKpKpPim']
linenames = ['Hlt2CharmHadDstp2D0Pip_D02'+x+'Turbo' for x in lnames]

#trigger filter
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT2_Code = "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02PimPimPipPipTurbo.*') | "+
    "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KpPimPimPipTurbo.*') |"+
    "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KmPimPipPipTurbo.*') |"+
    "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KmKpPimPipTurbo.*') |"+
    "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KmKmKpPipTurbo.*') |"+
    "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KmKpKpPimTurbo.*')"
    )

# DaVinci Configuration
OutputName = "DSTARD02HHHH"
from Gaudi.Configuration import *
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = OutputName+".Histos.root"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().RootInTES = '/Event/Turbo'

# =============================================================================
# configure the copy-stream algorithm
# =============================================================================
oname = OutputName + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh                = IOHelper       ()
copy               = ioh.outputAlgs (oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )

 
#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles([
#    'root://eoslhcb.cern.ch//eos/lhcb/user/m/mamartin/tmp/00099553_0000000%d_7.AllStreams.dst' % i for i in range(1,6)
#], clear=True)

