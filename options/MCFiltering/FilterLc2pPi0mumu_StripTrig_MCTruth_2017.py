"""
Author: Jolanta Brodzicka
Date: 19/06/2020
Options for building Stripping29r2p1 filtering on 
StrippingLc23MuLc2pmumupi0R(M) decision, Hlt1 and Hlt2,
and MCTruth matching
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
				"CloneDistCut" : [5000, 9e+99 ] }


from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MultiSelectionSequence
from Configurables import FilterDesktop
from Configurables import LoKiSvc
from Configurables import FilterInTrees, FilterDecays


# truth matching standard particle with added Stripping selections (except for PID cuts)

_stdMuons = DataOnDemand(Location = "Phys/StdLooseMuons/Particles")
_muonFilter = FilterDesktop('muonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
                            Code = "( mcMatch('[mu+]cc') & (PT > 300.*MeV) & (BPVIPCHI2() > 9) & (TRCHI2DOF < 4.) & (TRGHP < 0.4) )") 

MuonFilterSel = Selection(name = 'MuonFilterSel',
			  Algorithm = _muonFilter,
			  RequiredSelections = [ _stdMuons ])



_stdProtons = DataOnDemand(Location = "Phys/StdLooseProtons/Particles") 
_protonFilter = FilterDesktop('protonFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"],
			    Code = " (mcMatch('[p+]cc') & (PT > 300.*MeV) & (BPVIPCHI2() > 9) & (TRCHI2DOF < 4.) & (TRGHP < 0.4) )")

ProtonFilterSel = Selection(name = 'ProtonFilterSel',
			  Algorithm = _protonFilter,
			  RequiredSelections = [ _stdProtons ])


_stdPi0R = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
_Pi0RFilter = FilterDesktop('Pi0RFilter', 
                          Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"], 
                          Code = " (mcMatch('[pi0]cc') & (PT > 300*MeV) & (M > 110*MeV) & (M < 160*MeV) & ( CHILDCUT((CL > 0.2),1) ) & ( CHILDCUT((CL > 0.2),2) ) )")

Pi0RFilterSel = Selection(name = 'Pi0RFilterSel',
			  Algorithm = _Pi0RFilter,
			  RequiredSelections = [ _stdPi0R ])
	
		   
_stdPi0M = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")
_Pi0MFilter = FilterDesktop('Pi0MFilter',
			    Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"], 
                            Code = "( mcMatch('[pi0]cc') &  (PT > 500*MeV) & (M > 75*MeV) & (M < 195*MeV) )")

Pi0MFilterSel = Selection(name = 'Pi0MFilterSel',
			  Algorithm = _Pi0MFilter,
			  RequiredSelections = [ _stdPi0M ])



###### Lambda_c+ -> p+  mu+  mu-  pi0
_makeLc = CombineParticles("make_Lc",
			   DecayDescriptor = "[Lambda_c+ -> p+  mu+  mu-  pi0]cc",
			   CombinationCut =  "(ADAMASS('Lambda_c+') < 500 *MeV) & (ACHI2DOCA(1,4) < 10) & (ACHI2DOCA(2,4) < 10) & (ACHI2DOCA(3,4) < 10)",
			   #MotherCut = "mcMatch( '[Lambda_c+ => p+  mu+  mu- pi0]CC' ) | mcMatch( '[anti-Lambda_c- => p-  mu-  mu+ pi0]CC' )" )
                           MotherCut = "( mcMatch('[Lambda_c+ ==> p+  mu+  mu- pi0]CC') & (VFASPF(VCHI2) < 15) & ((BPVLTIME()*c_light) > 70*micrometer) & ( BPVIPCHI2() < 100 ) & (BPVDIRA > 0.999) )" ) #include intermediate resonances


selLcR = Selection("SelLcR",
		  Algorithm = _makeLc,
		  RequiredSelections = [ProtonFilterSel, MuonFilterSel, Pi0RFilterSel] )
seqR = SelectionSequence("MCTrueR", TopSelection=selLcR )

			   
selLcM = Selection("SelLcM",
		  Algorithm = _makeLc,
		  RequiredSelections = [ProtonFilterSel, MuonFilterSel, Pi0MFilterSel] )
seqM = SelectionSequence("MCTrueM", TopSelection=selLcM )


#
AllPi0 = MultiSelectionSequence("PromptLc2pmumupi0.StripTrig.MCTruth", Sequences = [seqR, seqM])


#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
				      stripDSTStreamConf,
				      stripDSTElements
				      )

SelDSTWriterElements = {
	'default'               : stripDSTElements(pack=enablePacking)
	     }

SelDSTWriterConf = {
#	'default'               : stripDSTStreamConf(pack=enablePacking)
        'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False,fileExtension='.dst')
	     }

dstWriter = SelDSTWriter( "MyDSTWriter",
			                             StreamConf = SelDSTWriterConf,
			                             MicroDSTElements = SelDSTWriterElements,
			                             OutputFileSuffix =' ',
			                             SelectionSequences = [AllPi0] #SelectionSequences = [seqR, seqM]
			                             )

#Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x62661709)

# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code   = "(HLT_PASS_RE('Hlt1TrackMVA.*Decision') | HLT_PASS_RE('Hlt1TwoTrackMVA.*Decision') | HLT_PASS_RE('Hlt1TrackMuon.*Decision') | HLT_PASS_RE('Hlt1DiMuonLowMass.*Decision'))",    
#
    HLT2_Code   = "(HLT_PASS_RE('Hlt2ExoticaDisplDiMuonNoPoint.*Decision') | HLT_PASS_RE('Hlt2DiMuonDetached.*Decision') | HLT_PASS_RE('Hlt2ExoticaDisplDiMuon.*Decision') | HLT_PASS_RE('Hlt2CharmHadInclSigc2PiLc2HHXBDT.*Decision') | HLT_PASS_RE('Hlt2RareCharmD2KMuMuOS.*Decision') )"      
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci, DumpFSR
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
#
DaVinci().DataType = "2017"                    
DaVinci().InputType = "DST"
#
DaVinci().appendToMainSequence( [ seqR.sequence() ] )
DaVinci().appendToMainSequence( [ seqM.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#
##for testing
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "PromptLc2pmumupi0.StripTrigID_2017_dumpfsr.check.output.txt"
##DumpFSR().AsciiFileName = "PromptLc2pphipi0.StripTrigID_2017_dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

##################################


