"""
Option file to filter simulated Turbo data based on the HLT2 trigger decision
(for Run 2). Relevant lines are:
  - Hlt2CharmHadXic0ToPpKmKmPipTurbo.*Decision
"""

# flake8: noqa

from Configurables import GaudiSequencer

# trigger filter
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters

trigfltrs = LoKi_Filters(
    HLT2_Code="  HLT_PASS_RE( 'Hlt2CharmHadXic0ToPpKmKmPipTurboDecision' )"
)

# DaVinci Configuration
from Gaudi.Configuration import *
from Configurables import DaVinci

DATA_YEAR = "2016"

output_name = "Xic0StSt_Xic0PiPi"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence("TrigFilters")
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"

DaVinci().RootInTES = {
    "2015": "/Event/Turbo",
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmmultibody/Turbo",
    "2018": "/Event/Charmmultibody/Turbo",
}[DATA_YEAR]


# configure the copy-stream algorithm
# =============================================================================

oname = output_name + ".HLTFILTER.MDST"
from GaudiConf import IOHelper

ioh = IOHelper()
copy = ioh.outputAlgs(oname, "InputCopyStream/INPUTCOPY")
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr

app = ApplicationMgr(OutStream=copy)


# Code to test the filter
# =============================================================================

# DaVinci().Simulation = True
# DaVinci().DataType = '2017'
# DaVinci().InputType = 'DST'
# DaVinci().EvtMax = -1
#
# from Configurables import DumpFSR
# DumpFSR().OutputLevel = 3
# DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
# DaVinci().MoniSequence += [ DumpFSR() ]
#
# from GaudiConf import IOHelper
# IOHelper('ROOT').inputFiles([
#
#    # EventType 11774004
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099093/0000/00099093_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [1, 10, 11, 16, 18, 20, 25, 26, 27]
#
#    # EventType 12365401
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099091/0000/00099091_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [6, 7, 8, 9, 10, 11, 13, 16, 18]
#
#    # EventType 27163100
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00099089/0000/00099089_000000{:02d}_7.AllStreams.dst'.format(n)
#        for n in [7, 10, 13, 14, 17, 18, 20, 23, 25]
#        for n in [7]
#    ],
#    clear=True)
