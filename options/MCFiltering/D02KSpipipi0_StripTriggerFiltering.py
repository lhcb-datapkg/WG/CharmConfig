"""
MC filtering for the stripping lines
  - B02DstarMuNu{;WS}Dst2D0Pi_D2KSPi0HH{LL;DD}{Resolved;Merged}BeautyLine
  - b2DstarMuXKsPiPiPi0{Resolved;Merged}{LL;DD}CharmFromBSemiLine
Further filters are applied on the HLT1 and HLT2 decisions.

The filter is designed for 2021 restripping campaign:
  - 2016: S28r2p1
  - 2017: S29r2p2
  - 2018: S34r0p2

***************************************************************************
*** DaVinci().DataType must be set in an option file before the filter! ***
***************************************************************************

author: tommaso.pajero@cern.ch
date: 10.12.2021
"""

from Gaudi.Configuration import *
MessageSvc().Format = '% F%30W%S%7W%R%T %0W%M'

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import DaVinci, StrippingTCK


# Stripping TCK FORMAT: DaVinci version XXrXpX, Stripping version YYrYpY -> TCK=0xXXXXYYYY
# for DaVinci-stripping matching see https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
if DaVinci().DataType == '2016':
  stripping='stripping28r2p1'
  stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports', TCK=0x44b42821)
elif DaVinci().DataType == '2017':
  stripping='stripping29r2p2'
  stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports', TCK=0x42b22922)
elif DaVinci().DataType == '2018':
  stripping='stripping34r0p2'
  stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports', TCK=0x44b13402)

config = strippingConfiguration(stripping)  # configuration dictionary from the database
archive = strippingArchive(stripping)       # line builders from the archive

streams = buildStreams(stripping=config, archive=archive)


my_lines = []
for pi0 in ['Resolved', 'Merged']:
  for ks in ['LL', 'DD']:
    my_lines.append('Strippingb2DstarMuXKsPiPiPi0{pi0}{ks}CharmFromBSemiLine'.format(
        pi0=pi0, ks=ks))
    for kind in ['', 'WS']:
      my_lines.append('StrippingB02DstarMuNu{kind}Dst2D0Pi_D2KSPi0HH{ks}{pi0}Beauty2CharmLine'.format(
          kind=kind, pi0=pi0, ks=ks))

lines_to_add = []
for stream in streams:
  if 'Charm' in stream.name() or 'Bhadron' in stream.name():
    for line in stream.lines:
      if line.name() in my_lines:
        line._prescale = 1.
        lines_to_add.append(line)
        print("Line '{}' found!".format(line.name()))
all_streams = StrippingStream('D02KSPIPIPI0.StripTrig')
all_streams.appendLines(lines_to_add)

sc = StrippingConf(Streams=[all_streams], MaxCandidates=2000, TESPrefix='Strip')

all_streams.sequence().IgnoreFilterPassed = False  # write out only selected events

# SelDSTWriter configuration
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements)

SelDSTWriterConf = {'default': stripMicroDSTStreamConf(pack=True, isMC=True, selectiveRawEvent=True)}
SelDSTWriterElements = {'default': stripMicroDSTElements(pack=True, isMC=True)}


# The following lines are needed only for 2011-2016
if DaVinci().DataType == '2016':
  # Items that might get lost when running the CALO+PROTO ReProcessing in DV
  caloProtoReprocessLocs = ['/Event/pRec/ProtoP#99', '/Event/pRec/Calo#99']
  # Make sure they are present on full DST streams
  SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter('MyDSTWriter',
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='',
                         SelectionSequences=sc.activeStreams())

# Trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters(
    HLT1_Code = "  HLT_PASS_RE('Hlt1TrackMuonMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1TrackMuon.*Decision')"
                "| HLT_PASS_RE('Hlt1TrackMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1TwoTrackMVA.*Decision')",
    HLT2_Code = "  HLT_PASS_RE('Hlt2Topo2Body.*Decision')"
                "| HLT_PASS_RE('Hlt2Topo3Body.*Decision')"
                "| HLT_PASS_RE('Hlt2Topo4Body.*Decision')"
                "| HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
                "| HLT_PASS_RE('Hlt2TopoMu3Body.*Decision')"
                "| HLT_PASS_RE('Hlt2TopoMu4Body.*Decision')"
                "| HLT_PASS_RE('Hlt2SingleMuon.*Decision')")

# DaVinci Configuration
DaVinci().EvtMax = -1
DaVinci().Simulation = True
DaVinci().ProductionType = 'Stripping'
DaVinci().HistogramFile = 'DVHistos.root'

DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name='TIMER')
TimingAuditor().TIMER.NameSize = 60

# Retention test
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = 'D02KSpipipi0_dumpfsr.txt'
DaVinci().MoniSequence += [DumpFSR()]
