"""
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamDDTurbo
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamLLTurbo
@author Yuehong Xie;Xiaopan Yang; Shiyang Li
@date 2018-08-10
"""
 
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, MultiSelectionSequence, MomentumScaling
 
line1name = 'Hlt2CharmHadLcp2LamKp_LamDDTurbo'
line2name = 'Hlt2CharmHadLcp2LamKp_LamLLTurbo'
 
line1 = AutomaticData( '/Event/Turbo/'+line1name+'/Particles')
line2 = AutomaticData( '/Event/Turbo/'+line2name+'/Particles')
 
sel1   = SelectionSequence( 'SEQ1' , line1)
sel2   = SelectionSequence( 'SEQ2' , line2)
selseq = MultiSelectionSequence('MULTI', Sequences = [sel1, sel2])

enablePacking =True
 
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

#
# Configuration of SelDSTWriter
#

SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }
 
SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Lc2ppik',
                         SelectionSequences= [ selseq ]                      
                         )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000019_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000030_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000037_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000045_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000050_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000059_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000065_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000073_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000082_3.AllStreams.dst'
#,'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000088_3.AllStreams.dst'
#], clear=True)
