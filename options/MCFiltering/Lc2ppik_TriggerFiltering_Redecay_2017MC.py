"""
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamKp_LamDDTurbo
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamKp_LamLLTurbo
@author Shiyang Li
@date 2021-10-26
"""

from Configurables import GaudiSequencer

# line names

hlt2_list = " HLT_PASS_RE('Hlt2CharmHadLcp2LamKp_LamDDTurbo.*') | HLT_PASS_RE('Hlt2CharmHadLcp2LamKp_LamLLTurbo.*')"

#trigger filter
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT2_Code = hlt2_list
    )


#
# DaVinci Configuration
#
OutputName = "Lc2ppik"
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import DstConf, TurboConf

DaVinci().DataType = "2017"
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = OutputName+"DVHistos.root"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().RootInTES = '/Event/Turbo/'
if 'Turbo' in DaVinci().properties().keys():
    DaVinci().Turbo = True
else:
    DstConf().Turbo = True
    TurboConf().PersistReco = True

# =============================================================================
# configure the copy-stream algorithm
# =============================================================================
oname = OutputName + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh                = IOHelper       ()
copy               = ioh.outputAlgs (oname, 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )

'''
# For local test
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]

from GaudiConf import IOHelper
IOHelper().inputFiles([
'/eos/lhcb/user/s/shiyang/sim09b_K/00057227_00000019_3.AllStreams.dst'
#'00132886_00000087_1.multi.dst'
], clear=True)
'''
