from Configurables import (DaVinci, StrippingTCK)
from StrippingConf.Configuration import (StrippingConf, StrippingStream)
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf, stripDSTElements)

stripping_version='stripping29r2p2'
custom_strip_stream = StrippingStream("XiPiPiDDD.Strip")
for stream in buildStreams(stripping=strippingConfiguration(stripping_version), archive=strippingArchive(stripping_version)):
    if 'charmcompleteevent' in stream.name().lower():
        for line in stream.lines:
            if "XcpToXiPipHp_PiDDD" in line.name():
                custom_strip_stream.appendLines([line])
sc = StrippingConf(Streams=[custom_strip_stream], MaxCandidates=2000, TESPrefix='Strip')
custom_strip_stream.sequence().IgnoreFilterPassed = False # so that we get only filtered events

SelDSTWriterElements = {'default': stripDSTElements()}
SelDSTWriterConf =     {'default': stripDSTStreamConf(selectiveRawEvent=True, fileExtension=".ldst")}
dstWriter = SelDSTWriter("MyDSTWriter", StreamConf=SelDSTWriterConf, MicroDSTElements=SelDSTWriterElements, SelectionSequences=sc.activeStreams())

# Stripping TCK FORMAT: DaVinci version XXrXpX, Stripping version YYrYpY -> TCK=0xXXXXYYYY
# for DaVinci-stripping matching see https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42b22922)

DaVinci(Simulation=True, ProductionType="Stripping")
DaVinci().appendToMainSequence([sc.sequence(), stck, dstWriter.sequence()])
