"""
Author: Jolanta Brodzicka
Date: 15/06/2020
Options for building Stripping21r0p2 filtering on 
StrippingDstarD2XGammaDstarD2KKGamma decision
and Hlt2 inclusive D*/Phi decision 
with tighten V Mass window
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping21r0p2'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

# turn off all the PID cuts in the stripping
largenumber = 1000.
smallnumber =-1000.
myconfig = config['DstarD2XGamma']
myconfig['CONFIG']['HighPIDK'] = largenumber # maximum PIDk for pions, was 0.0
myconfig['CONFIG']['LowPIDK'] = smallnumber # minimum PIDk for kaons, was 0.0

#VMassWindow MC:1020+-15 [In Selection:1020+-10]
myconfig['CONFIG']['MassLow_HH'] = 1005. # was 0
myconfig['CONFIG']['MassHigh_HH'] = 1035. # was 1865

# 'config' is some read-only database handle, make a new dictionary to pass to buildStream()
newconfig = { 'DstarD2XGamma' : myconfig }

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=newconfig, streamName=streamName, archive=archive)

streams = []

_charm         = quickBuild('CharmCompleteEvent')

MyLines = ['StrippingDstarD2XGammaDstarD2KKGammaLine', 'StrippingDstarD2XGammaDstarD2KKGamma_CNVLLLine', 'StrippingDstarD2XGammaDstarD2KKGamma_CNVDDLine']

_charm.lines[:] = [ x for x in _charm.lines if x.name() in MyLines ]
for line in _charm.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _charm )

AllStreams = StrippingStream("PromptD2PhiGamma.StripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

##rerun CALO in stripping21 production
##Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
## Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '',
                          SelectionSequences = sc.activeStreams()
                          )


#Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x409f0045)

# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code   = "(HLT_PASS_RE('Hlt1TrackAllL0.*Decision'))",    
    HLT2_Code   = "(HLT_PASS_RE('Hlt2CharmHadD02HHXDst.*Decision') | HLT_PASS_RE('Hlt2IncPhi.*Decision'))" 
)

#
# DaVinci Configuration
#
from Configurables import DaVinci, DumpFSR
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
#
DaVinci().DataType = "2012"                    
DaVinci().InputType = "DST"
#
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#for testing
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "PhiGamma_KKPi0_2012_TightCut_dumpfsr.check.output.VMass.2012.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
#


