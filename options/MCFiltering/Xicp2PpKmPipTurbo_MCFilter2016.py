"""
Option file to filter simulated Turbo data based on the HLT2 trigger decision
(for Run 2). Relevant lines are: 
  - Hlt2CharmHadXicpToPpKmPipTurboDecision

For 2016 MC.

Contact: r.oneil@cern.ch

"""

from Configurables import GaudiSequencer

from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT2_Code = "  HLT_PASS_RE( 'Hlt2CharmHadXicpToPpKmPipTurboDecision' )"
    )

from Gaudi.Configuration import *
from Configurables import DaVinci

DATA_YEAR = '2016'
output_name = "XicpToPpKmPip"

DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [ fltrSeq ]
DaVinci().HistogramFile = output_name + ".histos.root"

DaVinci().RootInTES = {
    '2015': '/Event/Turbo',
    '2016': '/Event/Turbo',
    '2017': '/Event/Charmspec/Turbo',
    '2018': '/Event/Charmspec/Turbo'
}[DATA_YEAR]


# configure the copy-stream algorithm
# =============================================================================

oname = output_name + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh = IOHelper()
copy = ioh.outputAlgs(oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )


