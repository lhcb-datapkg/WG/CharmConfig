# Author: Andrea Contu
# Date: 06/01/2016
#
# Modified : Davide Brundu
# Date : 01/11/2018
#
# Filter for Stripping28, using DstarPromptWithD02HHMuMu signal and control lines
#

# stripping version 28r1 --> DaVinci v41r4p4 
# (DV version not available on x86_64-slc6-gcc62-opt --> lb-run -c best DaVinci/v41r4p4 ...)

stripping='stripping28r1'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)

#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Merge into one stream and run in filtering mode
#
AllStreams = StrippingStream("D02HHMuMu.Strip")

linesToAdd = [ ]
for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if "DstarPromptWithD02HHLL" in line.name() or "DstarPromptWithD02HHMuMu" in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

# we get only filtered events
AllStreams.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
    }

# <-- No more necessary (?) --> #
#Items that might get lost when running the CALO+PROTO ReProcessing in DV
#caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
# Make sure they are present on full DST streams
#SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# <-- Stripping TCK --> # 
# TCK = 0xVVVVSSSS, where VVVV is the DaVinci version, and SSSS the Stripping number
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)

# <-- if test on stripped DST --> #
#from Configurables import EventNodeKiller
#event_node_killer = EventNodeKiller('StripKiller')
#event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']


# <-- DaVinci Configuration --> #
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1  # Number of events
DaVinci().DataType = "2016"
DaVinci().HistogramFile = "DVHistos-Trig.root"
#DaVinci().appendToMainSequence( [ event_node_killer ] ) # if test on stripped DST
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

# If you want to filter on trigger lines specified in trigfltrs above 
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

