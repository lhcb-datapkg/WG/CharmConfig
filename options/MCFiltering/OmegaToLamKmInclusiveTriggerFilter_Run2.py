"""
Option file to filter simulated Turbo data based on the HLT2 trigger decision (for Run 2).
Relevant lines are: Hlt2CharmHadOmm2LamKm_{LL,DD}LTurbo

@author Marian Stahl <marian.stahl@cern.ch>
@date 2020-10-21
"""
from Configurables import (ApplicationMgr, DaVinci)
from PhysConf.Filters import LoKi_Filters
from GaudiConf import IOHelper
# configure filter in DaVinci
trigfltrs = LoKi_Filters(HLT2_Code="HLT_PASS_RE('Hlt2CharmHadOmm2LamKm_LLLTurbo.*Decision') | HLT_PASS_RE('Hlt2CharmHadOmm2LamKm_DDLTurbo.*Decision')")
DaVinci().EventPreFilters = [trigfltrs.sequence('TrigFilters')]
DaVinci().RootInTES       = '/Event/Turbo'
#DaVinci().Turbo           = True #mstahl: it only works for 2017 (and 18?), see https://its.cern.ch/jira/browse/LHCBGAUSS-2164?focusedCommentId=3587223&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-3587223. Outputs seem to be the same without the Turbo flag, so removing it for all years.
DaVinci().Simulation      = True
# copy filtered events to output MDST
copy = IOHelper().outputAlgs('OMEGA2LK.HLTFILTER.MDST', 'InputCopyStream/INPUTCOPY')
copy[0].AcceptAlgs = DaVinci().EventPreFilters
ApplicationMgr(OutStream=copy)
