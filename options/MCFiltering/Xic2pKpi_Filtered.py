
#/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.MDST/00153100/0000/00153100_00000407_7.AllStreams.mdst


from Configurables import GaudiSequencer

#trigger filter                                                                                                                                                                                             
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code = "  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1.*TwoTrackMVA.*Decision')",
    HLT2_Code = "  HLT_PASS_RE( 'Hlt2CharmHadXicpToPpKmPipTurbo.*Decision')")


# DaVinci Configuration                                                                                                                                                                                     
from Gaudi.Configuration import *
from Configurables import DaVinci

output_name = "Xic2pKpi"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().RootInTES = '/Event/Turbo'


# configure the copy-stream algorithm                                                                                                                                                                       
# =============================================================================                                                                                                                             

oname = output_name + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh = IOHelper()
copy = ioh.outputAlgs(oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )

from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]
