"""
Stripping Filtering file for B->D0muX and B->DstmuX (Dst->D0pi) where D0->KsKK
@author Kamil Fischer
@date   2019-12-19
Module for selecting charm decays from semileptonic B decays.
Code for changed stripping line taken from: https://gitlab.cern.ch/lhcb/Stripping/blob/master/Phys/StrippingSelections/python/StrippingSelections/StrippingCharm/StrippingCharmFromBSemi.py
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Gaudi.Configuration import *
from Configurables import FilterDesktop, CombineParticles
from GaudiConfUtils.ConfigurableGenerators  import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseElectrons, StdLooseMuons, StdAllLooseMuons, StdLooseKaons, StdLooseProtons, StdNoPIDsPions, StdLooseMergedPi0, StdLooseResolvedPi0, StdAllNoPIDsPions, StdNoPIDsUpPions, StdLoosePhotons, StdAllLooseGammaLL, StdAllLooseGammaDD,StdNoPIDsDownPions
from Configurables import ConjugateNeutralPID
from PhysSelPython.Wrappers import MergedSelection


__all__ = ('DstarD2KShhBuilder',
           'makeDstar',
           'TOSFilter')

default_config = { 
    'NAME'        : "CharmFromBSemiWithTurbo",
    'WGs'         : ['Charm'],
    'STREAMS'     : [ 'Charm' ],
    'BUILDERTYPE' : 'DstarD2KShhBuilder',
    'CONFIG'      : {
    "GEC_nLongTrk"  :  10000        ## Global event cut on number of long tracks
    ,"TTSpecs"      : {"Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0.*TurboDecision%TOS":0}
    ,"HLT2"         : "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0.*TurboDecision')"
    ,'D0decaysPrescale'        : 1
}}

class DstarD2KShhBuilder(LineBuilder) :
    """
    Reconstruct D*+ -> D0 pi+, D0 -> KS0 h+ h- for h = K, pi
    (i.e. KSKK, KSKpi, KSpipi)
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    __confdict__={}

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config

        GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
                     "Preambulo": ["from LoKiTracks.decorators import *"]}


        DCuts = {"TTSpecs": config["TTSpecs"]}

        self.selPionloose = Selection( "SelPiloosefor" + name,
                                       Algorithm = self._pionlooseFilter("Piloosefor"+name),
                                       RequiredSelections = [StdNoPIDsPions])

        self.selKaonloose = Selection( "SelKloosefor" + name,
                                       Algorithm = self._kaonlooseFilter("Kloosefor"+name),
                                       RequiredSelections = [StdLooseKaons])


        self.combLooseKsLL = CombineParticles ("KsLLCombFor"+name,
                                               DecayDescriptor = "KS0 -> pi+ pi-" 
                                               , DaughtersCuts = { "pi+" : "(TRCHI2DOF<5.) & (MIPCHI2DV(PRIMARY)>20)" } 
                                               , CombinationCut = "(ADAMASS('KS0')<80*MeV) "
                                               , MotherCut = "(ADMASS('KS0')<50*MeV) & (VFASPF(VCHI2PDOF)<45) & (BPVLTIME() > 1.*ps) ")
                                                
        self.selLooseKsLL = Selection("SelKsLLFor"+name,
                                      Algorithm = self.combLooseKsLL,
                                      RequiredSelections = [ StdAllNoPIDsPions ])


        self.combLooseKsDD = CombineParticles ("KsDDCombFor"+name,
                                               DecayDescriptor = "KS0 -> pi+ pi-"
                                               , DaughtersCuts = { "pi+" : "(TRCHI2DOF<6.) & (P>1000*MeV) & (PT > 100.*MeV)" }
                                               , CombinationCut = "(ADAMASS('KS0')<100*MeV)"
                                               , MotherCut = "(ADMASS('KS0')<80*MeV) & (VFASPF(VCHI2PDOF)<45)  & (BPVVDZ > 200*mm)")

        self.selLooseKsDD = Selection("SelKsDDFor"+name,
                                      Algorithm = self.combLooseKsDD,
                                      RequiredSelections = [StdNoPIDsDownPions])

        self.selSlowPion = DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles')


        self.seld02KsKKLL = Selection( 'SelD02KsKKLLfor' + name,
                                       Algorithm = self._D02KsHHFilter('D02KsKKLLfor' + name),
                                       RequiredSelections = [self.selLooseKsLL,self.selKaonloose] )

        self.seld02KsKKDD = Selection( 'SelD02KsKKDDfor' + name,
                                       Algorithm = self._D02KsHHFilter('D02KsKKDDfor' + name),
                                       RequiredSelections = [self.selLooseKsDD,self.selKaonloose] )
        
        self.selDstar_2KsKKLL = makeDstar('DstarKKLLFor'+name,self.seld02KsKKLL,self.selSlowPion,DCuts)
        self.selDstar_2KsKKDD = makeDstar('DstarKKDDFor'+name,self.seld02KsKKDD,self.selSlowPion,DCuts)

        
        for line in [['DstarD02KsKKLL',self.selDstar_2KsKKLL],
                     ['DstarD02KsKKDD',self.selDstar_2KsKKDD],
                     ]:
            self.registerLine( StrippingLine('%s%sLine'%(line[0],name),
                                             prescale = config['D0decaysPrescale'],
                                             FILTER = GECs,
                                             HLT2 = config["HLT2"],
                                             selection = line[1]))

    def _D02KsHHFilter(self,_name):
        
        _d02KsHH = CombineParticles( name = _name,
                                     DecayDescriptor = "[D0 -> K- K+ KS0]cc",
                                     MotherCut = "(PT > 1750*MeV) & (CHI2VXNDOF < 20) & (BPVDIRA > 0.99) & (BPVLTIME() > 0.05*ps) & (D2DVVDCHI2(3) > 80)"

)
        return _d02KsHH

    def _kaonlooseFilter( self, _name ):
        _code = "  (TRCHI2DOF < 8) & (TRGHOSTPROB < 0.5) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 2) & (PIDK > -5)"
        _kal = FilterDesktop( name = _name, Code = _code )
        return _kal
        
    def _pionlooseFilter( self, _name ):
        _code = "  (TRCHI2DOF < 8) & (TRGHOSTPROB < 0.5) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 2) & (PIDK < 10)"
        _kal = FilterDesktop( name = _name, Code = _code )
        return _kal


def makeDstar(_name, inputD0,_softPi,DCuts) :
    _Dstar = CombineParticles("Comb_Dst" + _name,DecayDescriptors = ["D*(2010)+ -> D0 pi+", "D*(2010)- -> D0 pi-"],
                               MotherCut = '((M - CHILD(M,1)) > -10 *MeV) & ((M - CHILD(M,1)) < 170 *MeV)')

    selD = Selection ("Sel"+_name,Algorithm = _Dstar,RequiredSelections = [inputD0,_softPi])
    
    TOSD = TOSFilter(_name,selD,DCuts["TTSpecs"])
    return TOSD


def TOSFilter( name = None, sel = None, Specs = None ):
    if len(Specs) == 0:
        return sel
    """
    Function to return a selection object, filtering for TOS candidates from input selection
    """
    from Configurables import TisTosParticleTagger

    _filter = TisTosParticleTagger(name+"_TriggerTos")
    _filter.TisTosSpecs = Specs

    _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
    return _sel

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping34r0p1'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

charmlines = DstarD2KShhBuilder(name = default_config["NAME"],config = default_config['CONFIG'])

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("D02KSKK.StripTrig")

MyLines = [ 'Strippingb2D0MuXKsKKDDCharmFromBSemiLine', 'Strippingb2D0MuXKsKKLLCharmFromBSemiLine', 'Strippingb2DstarMuXKsKKDDCharmFromBSemiLine', 'Strippingb2DstarMuXKsKKLLCharmFromBSemiLine' ]


linesToAdd = []
for line in charmlines.lines():
    linesToAdd.append(line)

for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                line._prescale = 1.0
                linesToAdd.append(line)

#for l in linesToAdd:
#    print "added line "+ l.name()


AllStreams.appendLines(linesToAdd)


sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only filtered events

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf                                    )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36132100)

#
# DaVinci Configuration
#
from Configurables import DaVinci, DumpFSR

dv = DaVinci()
#fsr = DumpFSR()
#fsr.OutputLevel = 3
#fsr.AsciiFileName = "dumpfsr_check_output2016.txt"

DaVinci().Simulation = True
#DaVinci().EvtMax = 10000                        # Number of events
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
#DaVinci().MoniSequence += [DumpFSR()]
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr_check_output.txt"
