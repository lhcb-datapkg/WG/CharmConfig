"""
Turbo Filtering script
@author Kevin Maguire
@date   12-06-2017
"""

from Configurables import GaudiSequencer

line = 'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo'

#trigger filter
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence
the_line = AutomaticData( '/Event/Turbo/'+line+'/Particles')
selseq   = SelectionSequence( 'SEQ' , the_line ) 

#
# Configuration of SelDSTWriter for MDST
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }

dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='TurboFiltered',
                         SelectionSequences= [ selseq ]
                         )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
##testing
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
#from GaudiConf import IOHelper
###2016 Turbo03 KK MC 27163002
##IOHelper().inputFiles(['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00062514/0000/00062514_00000008_7.AllStreams.dst'], clear = True)
##2015 Turbo KPi MC 27163075
#IOHelper().inputFiles(['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00059434/0000/00059434_00000070_6.AllStreams.dst'], clear = True)
