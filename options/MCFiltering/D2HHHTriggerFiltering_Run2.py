"""
Option file to filter simulated Turbo data based on the HLT2 trigger decision
(for Run 2). Relevant lines are: 
  - Hlt2CharmHadDpTo{xxx}Turbo
  - Hlt2CharmHadiDpsTo{xxx}Turbo
where {xxx} = KmPipPip, KpPimPip, PimPipPip, KmKpKp, KpKpPim, for Dp
        and   KmKpPip, KpKpPim, KpPimPip, PimPipPip, KmKpKp, KmPipPip for Dsp
@author Erica Polycarpo <Erica.Polycarpo@cern.ch>
@date 2020-02-14
"""

from Configurables import GaudiSequencer

#trigger filter
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code = "  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')",
    HLT2_Code = "  HLT_PASS_RE( 'Hlt2CharmHadDpToKmPipPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDpToKpPimPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDpToKmKpPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDpToPimPipPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDpToKmKpKpTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDpToKpKpPimTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToKmKpPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToKpPimPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToPimPipPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToKmKpKpTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToKmPipPipTurbo.*Decision' )"
                "| HLT_PASS_RE( 'Hlt2CharmHadDspToKpKpPimTurbo.*Decision' )"
    )

# DaVinci Configuration
output_name = "D2HHH"
from Gaudi.Configuration import *
from Configurables import DaVinci
fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().RootInTES = '/Event/Turbo'


# configure the copy-stream algorithm
# =============================================================================

oname = output_name + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh = IOHelper()
copy = ioh.outputAlgs(oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )


# Code to test the filter
# =============================================================================

#DaVinci().Simulation = True
#DaVinci().DataType = '2016'
#DaVinci().InputType = 'DST'
#DaVinci().EvtMax = -1
#
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
#
#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles([
#    # 23163030 - DsKKPi - 2016 - MagDown - /Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.MDST/00076652/0000/00076652_000000{:02d}_7.AllStreams.mdst'.format(n)
#        for n in [2, 3, 10, 13, 18]
#    ],
#    clear=True)
