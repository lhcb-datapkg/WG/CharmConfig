"""
Stripping Filtering for Secondary D->4h samples. Also saving Turbo lines so it can be used for secondary contamination studies.
@author Nathan Jurik
@date   28-07-2020
"""

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Configurables import FilterDesktop, CombineParticles
from GaudiConfUtils.ConfigurableGenerators  import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLoosePions, StdLooseKaons, StdNoPIDsPions, StdNoPIDsKaons
from Configurables import ConjugateNeutralPID
from PhysSelPython.Wrappers import MergedSelection


#Roughly building the Turbo lines to avoid making too many candidates, will then filter on Turbo decision
d_prods = ["KmPimPipPip","KpPimPimPip","PimPimPipPip","KmKpPimPip","KmKmKpPip","KmKpKpPim"]
ttspecs = dict()
hlt2_req = ""
for prod in d_prods:
    iname = "Hlt2CharmHadDstp2D0Pip_D02{}.*TurboDecision".format(prod)
    ttspecs["{}%TOS".format(iname) ] = 0
    new_req = "HLT_PASS_RE('{}')".format(iname)
    if hlt2_req=="":
        hlt2_req = new_req
    else:
        hlt2_req += " | {}".format(new_req)

default_config = { 
    'NAME'        : "CharmFromBSemiWithTurbo",
    'WGs'         : ['Charm'],
    'STREAMS'     : [ 'Charm' ],
    'BUILDERTYPE' : 'DstarD2hhhhBuilder',
    'CONFIG'      : {
    "GEC_nLongTrk"  :  10000        ## Global event cut on number of long tracks
    ,"TTSpecs"      : ttspecs
    ,"HLT2"         : hlt2_req
    ,'D0decaysPrescale'        : 1
}}



class DstarD2hhhhBuilder(LineBuilder) :
    """
    Reconstruct D*+ -> D0 pi+, D0 -> Kpipipi
    """

    __configuration_keys__ = default_config['CONFIG'].keys()

    __confdict__={}

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)
        self.__confdict__=config


        d_cuts = {"TTSpecs": config["TTSpecs"]}

        detached_pion = Selection( "SelPiloosefor" + name,
                                   Algorithm = self.detached_pion_filter("Piloosefor"+name),
                                   RequiredSelections = [StdNoPIDsPions])

        detached_kaon = Selection( "SelKloosefor" + name,
                                   Algorithm = self.detached_kaon_filter("Kloosefor"+name),
                                   RequiredSelections = [StdNoPIDsKaons])


        tag_pion = DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles')

        d02kpipipi_comb = self.d02hhhh_filter(['[D0 -> K- pi+ pi+ pi-]cc'],'CharmHadD02HHHH_D02KPiPiPi')


        d02kpipipi = Selection("SelD2Kpipipi"+name,
                                 Algorithm = d02kpipipi_comb,
                                 RequiredSelections = [detached_pion,detached_kaon])


        self.dst_d0pi = self.tag_decay('Dstar2DpiFor'+name,d02kpipipi,tag_pion,d_cuts)
        
        for line in [['DstarD0pi_D02Kpipipi',self.dst_d0pi]]:
            self.registerLine( StrippingLine('%s_%sLine'%(name,line[0]),
                                             prescale = config['D0decaysPrescale'],
                                             HLT2 = config["HLT2"],
                                             selection = line[1]))



    def d02hhhh_filter( self , _decayDescriptors,name):



        c12 = (" ( AM < (2100*MeV - (139.5 + 139.5) * MeV) ) " +
               "&( ADOCA(1,2) < 100.0 * mm ) " +
               "&( ACHI2DOCA(1,2) < 10.0 ) " )
        c123 =(" ( AM < (2100*MeV - (139.5) * MeV) ) " +
               "&( ADOCA(1,3) < 100.0 * mm ) " +
               "&( ADOCA(2,3) < 100.0 * mm ) " +
               "&( ACHI2DOCA(1,3) < 10.0 ) " +
               "&( ACHI2DOCA(2,3) < 10.0 ) " )
        cc =  (" (in_range( 1700 * MeV, AM, 2100*MeV )) " +
               "&( (APT1+APT2+APT3+APT4) > 1800 * MeV )" +
               "&( AP > 25000 * MeV )" +
               "&( ADOCA(1,4) < 100.0 * mm ) " +
               "&( ADOCA(2,4) < 100.0 * mm ) " +
               "&( ADOCA(3,4) < 100.0 * mm ) " +
               "&( ACHI2DOCA(1,4) < 10.0 ) " +
               "&( ACHI2DOCA(2,4) < 10.0 ) " +
               "&( ACHI2DOCA(3,4) < 10.0 ) " )
        mc =    ("(CHI2VXNDOF < 12.0)" +
                 " & (PT > 2000 * MeV )" +
                 " & (P  > 30000 * MeV )"+
                 " & (BPVLTIME() > 0.1*picosecond )" +
                 " & (BPVVDCHI2 > 25 )"  )  



        _d02hhhh_comb = DaVinci__N4BodyDecays(DecayDescriptors = _decayDescriptors,
                                         Combination12Cut = c12, Combination123Cut = c123,
                                         CombinationCut = cc,
                                         MotherCut = mc)


        return _d02hhhh_comb

    def detached_kaon_filter( self, name ):
        _code = "  (TRCHI2DOF < 3) & (TRGHOSTPROB < 0.4) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 3) & (PIDK > 5) & (PT > 200*MeV) "
        _kal = FilterDesktop( name = name, Code = _code )
        return _kal
        
    def detached_pion_filter( self, name ):
        _code = "  (TRCHI2DOF < 3) & (TRGHOSTPROB < 0.4) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 3) & (PIDK < 5) & (PT > 200*MeV) "
        _kal = FilterDesktop( name = name, Code = _code )
        return _kal


    def tag_decay(self,name, inputD0,_softPi,d_cuts) :


        cc =    ('in_range( -5.0*MeV, (AM - AM1 - AM2), 190.0*MeV )')
        mc =    ("(VFASPF(VCHI2PDOF) < 15.0)" +
                 "& in_range( -5.0*MeV, (M - M1 - M2), 170.0*MeV )")

        _Dstar = CombineParticles("Comb_Dst" + name,DecayDescriptors = ["[D*(2010)+ -> D0 pi+]cc", "[D*(2010)- -> D0 pi-]cc"],
                                  CombinationCut = cc,
                                  MotherCut = mc)
        
        selD = Selection ("Sel"+name,Algorithm = _Dstar,RequiredSelections = [inputD0,_softPi])
        TOSD = self.TOSFilter(name,selD,d_cuts["TTSpecs"])
        return TOSD


    def TOSFilter(self, name = None, sel = None, Specs = None ):
        if len(Specs) == 0:
            return sel
        """
        Function to return a selection object, filtering for TOS candidates from input selection
        """
        from Configurables import TisTosParticleTagger
        
        _filter = TisTosParticleTagger(name+"_TriggerTos")
        _filter.TisTosSpecs = Specs

        _sel = Selection("Sel" + name + "_TriggerTos", RequiredSelections = [ sel ], Algorithm = _filter )
        return _sel


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
  "CloneDistCut" : [5000, 9e+99 ] }
#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping24r2'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_filterlines = quickBuild('Charm')

# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("D02hhhh.Strip")
# Select lines by name
MyLines = [ 'Strippingb2DstarMuXK3PiCharmFromBSemiLine',
            'Strippingb2D0MuXK3PiCharmFromBSemiLine',
            'Strippingb2DstarMuX4PiCharmFromBSemiLine',
            'Strippingb2D0MuX4PiCharmFromBSemiLine',
            'Strippingb2DstarMuX3KPiCharmFromBSemiLine',
            'Strippingb2D0MuX3KPiCharmFromBSemiLine',
            'Strippingb2DstarMuX2K2PiCharmFromBSemiLine',
            'Strippingb2D0MuX2K2PiCharmFromBSemiLine'
        ]


##here
charmlines = DstarD2hhhhBuilder(name = default_config["NAME"],config = default_config['CONFIG'])
for line in charmlines.lines():
    MyStream.appendLines([line])


for line in _filterlines.lines :
    line._prescale = 1.0
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 
    
# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    AcceptBadEvents = False,
                    TESPrefix = 'Strip',
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )


#
# Configuration of SelDSTWriter
#
enablePacking = True
SelDSTWriterElements = {
    'default'              : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }


SelDSTWriterConf = {
    'default'              : stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# DaVinci Configuration
from Configurables import DaVinci
DaVinci().InputType = 'MDST'
DaVinci().Simulation = True
#DaVinci().PrintFreq  = 200
DaVinci().EvtMax = -1 
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

