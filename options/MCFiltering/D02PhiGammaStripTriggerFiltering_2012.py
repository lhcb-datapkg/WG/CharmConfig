"""
Author: Jolanta Brodzicka
Date: 11/10/2016 (debugged version 02/12/2016)
Options for building Stripping21 filtering on 
StrippingD2XGammaStripping_Dst2D0Pi decision
and Hlt2IncPhi or Hlt2 inclusive D* decision (TCK=0x409f0045)
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Configuration of stripping line
#

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from StandardParticles import StdLooseMuons, StdLooseKaons
from Configurables import FilterDesktop, CombineParticles, OfflineVertexFitter, LoKi__HDRFilter 
from GaudiKernel.PhysicalConstants import c_light


def makeMySelection(name=''):
    '''
    Arguments:
    name: name of the Selection
    '''
    #Daughters
    looseKaons = DataOnDemand(Location = 'Phys/StdAllNoPIDsKaons/Particles')
    loosePhotons = DataOnDemand(Location = 'Phys/StdLooseAllPhotons/Particles')
    loosePions = DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles')

    
    # Select Phi following StdLoosePhi2KK but using NoPIDsKaons
    recoPhi = CombineParticles(name+"Phi2KK")
    recoPhi.DecayDescriptor = 'phi(1020) -> K+ K-'
    recoPhi.DaughtersCuts = {"K+": "(TRCHI2DOF < 2.5 ) & (MIPCHI2DV(PRIMARY) > 25.0) & (PT > 500.0)","K-": "(TRCHI2DOF < 2.5 ) & (MIPCHI2DV(PRIMARY) > 25.0) & (PT > 500.0)"} 
    recoPhi.CombinationCut =  "(AM < 1100.*MeV) & (ADOCACHI2CUT(30, ''))"
    recoPhi.MotherCut = "( (VFASPF(VCHI2) < 25.0) & (VFASPF(VCHI2/VDOF) < 16.0) & (ADMASS('phi(1020)') < 50.0*MeV) & (MM > 1000.0*MeV))"

    phi_selection = Selection(name+'SelPhi2KK', Algorithm = recoPhi, RequiredSelections = [ looseKaons ])
    

    # Select D0 a la stripping 
    recoD0 = CombineParticles(name+"D2PhiGamma")
    recoD0.DecayDescriptor = 'D0 -> phi(1020) gamma'
    recoD0.DaughtersCuts = { "gamma" : " ( PT > 1500 * MeV )" } #try looser than default 1.7GeV
    recoD0.CombinationCut = "(ADAMASS('D0')<220.*MeV)"
    recoD0.MotherCut = "( (ADMASS('D0')<200.0*MeV) & (PT> 1000.0) )"

    D0_selection = Selection(name+"SelD2PhiGamma", Algorithm = recoD0, RequiredSelections = [ loosePhotons, phi_selection ])

    #select D* a la stripping
    recoDst = CombineParticles(name+"Dst2D0Pi")
    recoDst.DecayDescriptors = ['D*(2010)+ -> D0 pi+' , 'D*(2010)- -> D0 pi-']
    recoDst.DaughtersCuts = { "pi+" : " (TRCHI2DOF < 5)", "pi-" : " (TRCHI2DOF < 5)" }
    recoDst.CombinationCut = "((AM - AM1) < 1.1*160.0*MeV)"
    recoDst.MotherCut = "( (VFASPF(VCHI2/VDOF) < 25.0) & ((M - M1) < 160.0*MeV) & ((M - M1) > 130.0*MeV) )"

    Dst_selection = Selection(name+"SelDst2D0Pi", Algorithm = recoDst, RequiredSelections = [ loosePions, D0_selection ])

    return Dst_selection


from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

class MyLineBuilderConf(LineBuilder):

    __configuration_keys__ = ( 'Postscale', 'Prescale')

    config_default={
        'Postscale'  :1,
        'Prescale'   :1,
        }

    def __init__(self , name='' , config=None) :
        LineBuilder.__init__(self , name , config=config)
        self.Dst_selection = makeMySelection(name)
        self.makeLine(name , config)

    

    def makeLine(self, name , config) :
        line = StrippingLine(name+'PromptD2PhiGamma'+'Line',
                             prescale=1.,
                             selection=self.Dst_selection)
        self.registerLine( line )


config = { 'Postscale'  :1, 'Prescale'   :1}

myBuilder = MyLineBuilderConf('MyLoose', config, )



#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping21'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_charm         = quickBuild('Charm')

MyLines = []

_charm.lines[:] = [ x for x in _charm.lines if 'PromptD2PhiGamma' in x.name() ]
for line in _charm.lines:
    line._prescale = 1.0
    print "charm has a line called " + line.name()
streams.append( _charm )

AllStreams = StrippingStream("PromptD2PhiGamma.StripTrig")

for stream in streams:
    AllStreams.appendLines(stream.lines)

## Add also my line
AllStreams.appendLines( myBuilder.lines() )
###

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }

##rerun CALO in stripping21 production
##Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
## Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

#Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x409f0045)


# Include trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code   = "(HLT_PASS_RE('Hlt1TrackPhoton.*Decision') | HLT_PASS_RE('Hlt1TrackAllL0.*Decision')) & (HLT_PASS_RE('Hlt2CharmHadD02HHXDst.*Decision') | HLT_PASS_RE('Hlt2IncPhi.*Decision')) " 
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

##testing
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
#DaVinci().EvtMax = 30000           
#DaVinci().DataType = "2012"
#from GaudiConf import IOHelper
#IOHelper().inputFiles(['PFN:/afs/cern.ch/work/j/jbrodzic/phigamma/mc/00031649_00000060_1.allstreams.dst'],clear = True)


