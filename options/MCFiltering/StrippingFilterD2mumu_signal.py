# $Id: $
# Test your line(s) of the stripping
#  
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
confname='DstarD02xx' #FOR USERS    

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
#from StrippingSelections import buildersConf
#confs = buildersConf()
#from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
#confs[confname]["CONFIG"]["SigmaPPi0CalPrescale"] = 0.5 ## FOR USERS, YOU ONLY NEED TO QUICKLY MODIFY CutName and NewValue (no need to recompile the package but please update the default_config before committing)

sys.path.append(os.environ["CHARMCONFIGOPTS"]+"/MCFiltering")
from StrippingDstarD02xx_MC import default_config, StrippingDstarD02xxConf

#streams = buildStreamsFromBuilder(confs,confname)

from StrippingConf.Configuration import StrippingConf, StrippingStream
#select only signal line
default_config['CONFIG']['PrescalepipiBox'] = 0
default_config['CONFIG']['PrescalemumuBox'] = 1.
default_config['CONFIG']['PrescaleKpiBox']  = 0.
default_config['CONFIG']['PrescaleemuBox']  = 0.
default_config['CONFIG']['PrescaleeKBox']   = 0.
default_config['CONFIG']['PrescaleepiBox']  = 0.
default_config['CONFIG']['PrescalepimuBox'] = 0.
default_config['CONFIG']['PrescaleKmuBox']  = 0.
default_config['CONFIG']['Prescalepipi_untagged_Box'] = 0.0
default_config['CONFIG']['Prescalemumu_untagged_Box'] = 0.0
default_config['CONFIG']['PrescaleKpi_untagged_Box']  = 0.0
default_config['CONFIG']['Prescalepimu_untagged_Box'] = 0.0
default_config['CONFIG']['PrescaleKmu_untagged_Box']  = 0.0
default_config['CONFIG']['PrescaleKpi_untagged_BoxMB'] = 0.
default_config['CONFIG']['Prescalepipi_untagged_BoxMB'] = 0.
default_config['CONFIG']['PrescaleKpi_untagged_BoxMBTrEff'] = 0.


builder = StrippingDstarD02xxConf(default_config['NAME'],default_config['CONFIG'])
mystreamname = "D02MUMU"
stream = StrippingStream(mystreamname)
stream.appendLines( builder.lines() )




mdstStreams = [ mystreamname]
dstStreams  = [ ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = [stream],
                    MaxCandidates = 2000,
                    TESPrefix = stripTESPrefix,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

## we get only filtered events
#stream.sequence().IgnoreFilterPassed = False
#
# Configure the dst writers for the output
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True,isMC=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking,isMC=True)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    mystreamname            : mdstElements
    }

SelDSTWriterConf = {
    mystreamname             : mdstStreamConf,
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )


from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)

#Configure DaVinci
DaVinci().Simulation = True
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 10000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

