from Configurables import (DaVinci, StrippingTCK)
from StrippingConf.Configuration import (StrippingConf, StrippingStream)
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from DSTWriters.Configuration import (SelDSTWriter, stripDSTStreamConf, stripDSTElements)

stripping_version='stripping28r2p2'
custom_strip_stream = StrippingStream("XiXi0MuNu.Strip")
StrippingLines = StrippingLines = [
'StrippingXic0ToXiMuNu_DDDLine',
'StrippingXic0ToXiMuNu_DDLLine',
'StrippingXic0ToXiMuNu_LLLLine',
'StrippingXic0ToXiMuNu_WS_DDDLine',
'StrippingXic0ToXiMuNu_WS_DDLLine',
'StrippingXic0ToXiMuNu_WS_LLLLine'
]
for stream in buildStreams(stripping=strippingConfiguration(stripping_version), archive=strippingArchive(stripping_version)):
    if 'charmcompleteevent' in stream.name().lower():
        for line in stream.lines:
            if line.name() in StrippingLines:
                custom_strip_stream.appendLines([line])
sc = StrippingConf(Streams=[custom_strip_stream], MaxCandidates=2000, TESPrefix='Strip')
custom_strip_stream.sequence().IgnoreFilterPassed = False # so that we get only filtered events

SelDSTWriterElements = {'default': stripDSTElements()}
SelDSTWriterConf =     {'default': stripDSTStreamConf(selectiveRawEvent=True, fileExtension=".ldst")}
dstWriter = SelDSTWriter("MyDSTWriter", StreamConf=SelDSTWriterConf, MicroDSTElements=SelDSTWriterElements, SelectionSequences=sc.activeStreams())

# Stripping TCK FORMAT: DaVinci version XXrXpX, Stripping version YYrYpY -> TCK=0xXXXXYYYY
# for DaVinci-stripping matching see https://twiki.cern.ch/twiki/bin/view/LHCb/Offprod
# DV v44r11p6 SV 28r2p2 --> TCK = 0x46702822
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x46702822)

DaVinci(Simulation=True, ProductionType="Stripping")
DaVinci().appendToMainSequence([sc.sequence(), stck, dstWriter.sequence()])
