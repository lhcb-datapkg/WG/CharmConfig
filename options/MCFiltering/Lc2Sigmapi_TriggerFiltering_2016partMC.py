"""
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamDDTurbo
Turbo Filtering file Monte Carlo for Hlt2CharmHadLcp2LamPip_LamLLTurbo
@author Shiyang Li
@date 2019-01-15
"""
 
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, MultiSelectionSequence
 
line1name = 'Hlt2CharmHadLcp2LamPip_LamDDTurbo'
line2name = 'Hlt2CharmHadLcp2LamPip_LamLLTurbo'
 
line1 = AutomaticData( '/Event/Turbo/'+line1name+'/Particles')
line2 = AutomaticData( '/Event/Turbo/'+line2name+'/Particles')
 
sel1   = SelectionSequence( 'SEQ1' , line1)
sel2   = SelectionSequence( 'SEQ2' , line2)
selseq = MultiSelectionSequence('Lc2ppipi.MULTI', Sequences = [sel1, sel2])

enablePacking =True
 
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }
 
SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }
dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='000000',
                         SelectionSequences= [ selseq ]                      
                         )
'''
from Configurables import DumpFSR, DaVinci
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
DaVinci().MoniSequence += [ DumpFSR() ]
'''
#
# DaVinci Configuration
#
from Configurables import DaVinci
#DaVinci().InputType = 'DST'
#DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
'''
from GaudiConf import IOHelper
IOHelper().inputFiles([
'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000021_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000020_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000019_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000018_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000017_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000016_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000015_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000014_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000013_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000008_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000031_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000030_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000029_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000028_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000027_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000026_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000025_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000024_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000023_7.AllStreams.mdst'
,'/afs/cern.ch/user/s/shiyang/part_mc/00065622_00000022_7.AllStreams.mdst'
], clear=True)
'''
