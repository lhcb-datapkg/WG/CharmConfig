"""
Stripping & trigger filtering file for Xib -> (Xic+ -> p+ K- pi+) mu- nu MC
@author Daniele Marangotto
@date   2021-03-31
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# --- Stripping filtering ---

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#stripping 29r2 built with DaVinci v42r9p2
stripping='stripping29r2'

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_filterlines = quickBuild('Semileptonic')

MyStream = StrippingStream("Xib2Xicmunu_Xic2pKpi.StripTrig")

MyLines= ['StrippingB2DMuNuX_Xic']

for line in _filterlines.lines :
    if line.name() in MyLines:
        print "APPENDING LINE {0}".format(line.name())
        MyStream.appendLines( [ line ] ) 

# Set prescale to 1.0
for line in _filterlines.lines:
    line._prescale = 1.0

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
	            AcceptBadEvents = False,
	            BadEventSelection = filterBadEvents
                    )

MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out


#
# Configuration of SelDSTWriter
#

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )

SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }

SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

#Items that get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42922920)

# --- Trigger filtering ---

from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
       L0DU_Code   = "L0_CHANNEL_RE('Muon')",
       HLT1_Code = "HLT_PASS_RE('Hlt1TrackMuonDecision')",
       HLT2_Code   = "HLT_PASS_RE('Hlt2TopoMu2BodyDecision') | HLT_PASS_RE('Hlt2TopoMu3BodyDecision') | HLT_PASS_RE('Hlt2TopoMu4BodyDecision')"
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().DataType = '2017'
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
