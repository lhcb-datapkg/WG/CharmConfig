"""
Stripping Filtering file for B->D*munuX where D0->K3pi
@author Philip Hunt
@date   2013-07-26
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

# import our modified stripping line module (no default PID, IP chi2 and pt cut son D daughters)
import CharmConfig.StrippingB2DMuNuX_AllNoPIDsHadrons as myStripModule

stripping='stripping20r1'
stripConf='B2DMuNuX'

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
# Make the necessary changes to the configuration dictionary
cnf=config[stripConf]['CONFIG']

# remove the kaon cuts (up to the value in StdLooseKaons)
cnf['KaonPIDK']=-1.0e6
cnf['KaonPIDKTight']=-1.0e6

# remove the pion cuts (up to the value in StdLoosePions)
cnf['PionPIDK']=1.0e6
cnf['PionPIDKTight']=1.0e6

# remove the D daughter IP chi2 cuts
cnf['MINIPCHI2']=0

myConf=myStripModule.B2DMuNuXAllLinesConf(stripConf, cnf)

streams = []

###########################################
###########################################

# Lines demanded by analyst (Philip Hunt)
#Strippingb2D0MuXK3PiB2DMuNuX

_filterlines = StrippingStream('Semileptonic')
_filterlines.appendLines(myConf.lines())

# Select lines you want
# Stream name will control name in book-keeping - make it something descriptive
MyStream = StrippingStream("B2DstMuNuX_D02K3Pi.StripTrig")
# Select lines by name
MyLines = [ 'Strippingb2D0MuXK3PiDstB2DMuNuXLine' ]

for line in _filterlines.lines :
    if line.name() in MyLines:
        line._prescale = 1.0
        MyStream.appendLines( [ line ] ) 

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False # so that we get only selected events written out

###########################################
###########################################



from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )


#
# Configuration of SelDSTWriter (DST)
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

#
# Uncomment to use MicroDST writing
#
# from DSTWriters.Configuration import (stripMicroDSTStreamConf,
#                                      stripMicroDSTElements
#                                      )
## SelDSTWriterElements = {
##     'default'              : stripMicroDSTElements(pack=True,
##                                                    isMC=True)
##     }


## SelDSTWriterConf = {
##     'default'              : stripMicroDSTStreamConf(pack=True,
##                                                      isMC=True)
##     }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )


#----------Include trigger filtering---------------------------

# Bring in the filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

    HLT_Code   = "HLT_PASS_RE('Hlt1Track.*Decision') & (HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2SingleMuon.*Decision'))"

    )
#----------------------------------------------------------------

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

#
# Need to add bank-killer for testing
##from Configurables import EventNodeKiller
##eventNodeKiller = EventNodeKiller('StripKiller')
##eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
##DaVinci().appendToMainSequence( [ eventNodeKiller ] )   # Kill old stripping banks first
#

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
#DaVinci().UseTrigRawEvent=True # 


# Bring in some local test data
## Signal test data
## B0->D*munu(D0->K3pi) MC
##EventSelector().Input   = [
##    "DATAFILE='PFN:/data/lhcb/users/hunt/ALLSTREAMS.DST/Sim08-Reco14-Stripping20Flagged/11676001/00025040_00000030_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
##    "DATAFILE='PFN:/data/lhcb/users/hunt/ALLSTREAMS.DST/Sim08-Reco14-Stripping20Flagged/11676001/00025040_00000115_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'",
##    "DATAFILE='PFN:/data/lhcb/users/hunt/ALLSTREAMS.DST/Sim08-Reco14-Stripping20Flagged/11676001/00025040_00000118_1.allstreams.dst' TYP='POOL_ROOTTREE' OPT='READ'"
##]
