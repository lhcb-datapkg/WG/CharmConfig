"""                                                                                                                                                                                                         
Turbo Filtering script for Monte Carlo for Hlt2CharmHadLcpToPpKmPipTurbo (target line)                                                                                                                      
and a family of other lines that might be interested in these productions.                                                                                                                                  
@author Louis Henry                                                                                                                                                                                         
@date   2018-11-29                                                                                                                                                                                          
"""

from Configurables import GaudiSequencer

#trigger filter                                                                                                                                                                                             
from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code = "  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1CalibTracking.*Decision')",
    HLT2_Code = "  HLT_PASS_RE( 'Hlt2CharmHadLcpToPpKmPipTurbo.*Decision')")


# DaVinci Configuration                                                                                                                                                                                     
from Gaudi.Configuration import *
from Configurables import DaVinci

output_name = "Lc2pKpi"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().RootInTES = '/Event/Turbo'


# configure the copy-stream algorithm                                                                                                                                                                       
# =============================================================================                                                                                                                             

oname = output_name + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh = IOHelper()
copy = ioh.outputAlgs(oname , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )
