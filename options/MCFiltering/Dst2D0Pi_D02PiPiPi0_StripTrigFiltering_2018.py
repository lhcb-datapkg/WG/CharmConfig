'''
Stripping filtering file for D* -> D0 pi where D0 -> pi pi pi0 using 2018 conditions
@author Niall McHugh
@date   2021-07-16
'''

# in case DaVinci version is different from that used in the stripping
from CommonParticlesArchive import CommonParticlesArchiveConf
stripping='stripping34'
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Merge into one stream
#
AllStreams = StrippingStream("Dst2D0Pi_D02PiPiPi0.StripTrig")

MyLines = ['StrippingDstarD0ToHHPi0_pipipi0_M_Line', 'StrippingDstarD0ToHHPi0_pipipi0_R_Line',
           'StrippingDstarD0ToHHPi0_Kpipi0_M_Line', 'StrippingDstarD0ToHHPi0_Kpipi0_R_Line',
           'StrippingDstarD0ToHHPi0_KKpi0_M_Line', 'StrippingDstarD0ToHHPi0_KKpi0_R_Line']

linesToAdd = []
for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if line.name() in MyLines:
                print "!!!!!!!!!"
                print line.name()
                print "!!!!!!!!!"
                line._prescale = 1.0
                linesToAdd.append(line)

AllStreams.appendLines(linesToAdd)


sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

AllStreams.sequence().IgnoreFilterPassed = False # so that we get only filtered events

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf                                    )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, isMC=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44403400)

#
# Trigger filtering
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT1_Code = "HLT_PASS_RE('Hlt1TrackMVADecision') | HLT_PASS_RE('Hlt1TwoTrackMVADecision')"
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
