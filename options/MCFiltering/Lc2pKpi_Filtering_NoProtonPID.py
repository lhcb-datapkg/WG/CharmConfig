from GaudiConf import IOHelper
from Configurables import DaVinci
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllNoPIDsKaons as Kaons
from StandardParticles import StdAllNoPIDsProtons as Protons
from PhysConf.Selections import SimpleSelection, SelectionSequence
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators

# =========================#
# == Build Lc candidates ==#
# =========================#
# Cuts on the Lc daughters
lc_decay_products = {
    "p+": "(PT > 1000*MeV) & (P > 1000*MeV) & (TRCHI2DOF<3) & (MIPCHI2DV(PRIMARY)>6) & (TRGHOSTPROB<1)",
    "K-": "(PT > 400*MeV) & (P > 1000*MeV) & (TRCHI2DOF<3) & (PIDK>5) & (MIPCHI2DV(PRIMARY)>9) & HASRICH & (TRGHOSTPROB<1)",
    "pi+": "(PT > 400*MeV) & (P > 1000*MeV) & (TRCHI2DOF<3) & (PIDK<-5) & (MIPCHI2DV(PRIMARY)>9) & HASRICH & (TRGHOSTPROB<1)",
}

# Cuts on the combined pKpi candidates
lc_comb = "(ADAMASS('Lambda_c+') < 85*MeV) & (APT>2000*MeV) & (ANUM(MIPCHI2DV(PRIMARY)>12)>=2) & (ANUM(MIPCHI2DV(PRIMARY)>16)>=1)"

# Cuts on the Lc candidate
lc_mother = "(ADMASS('Lambda_c+') < 75*MeV) & (BPVIPCHI2()<10) & (BPVDIRA>0.99995) & (BPVLTIME()>0.15*ps) & (CHI2VXNDOF<10) & (ADWM('D_s+', WM('K+','K-','pi+'))>20*MeV) & (ADWM('D+', WM('pi+','K-','pi+'))>20*MeV) & (WM('pi+','K-','pi+')>1735*MeV) & (WM('K+','K-','pi+')>1875*MeV)"

# Create the Lc candidate with cuts
lc_sel = SimpleSelection(
    "Lc2pKpi_Sel_NoProtonPID",
    ConfigurableGenerators.CombineParticles,
    [Protons, Kaons, Pions],
    DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
    DaughtersCuts=lc_decay_products,
    CombinationCut=lc_comb,
    MotherCut=lc_mother,
)
# Build
lc_seq = SelectionSequence("Lc2pKpi_NoProtonPID", TopSelection=lc_sel)

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

# Configuration of SelDSTWriter
enablePacking = True
SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default': stripDSTStreamConf(pack=enablePacking)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Lc2pKpi',
                         SelectionSequences= [ lc_seq ]                       
                         )

# =========================#
# === Configure DaVinci ===#
# =========================#
output_name = "Lc2pKpi"
DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().Simulation = True
#DaVinci().InputType = "DST"
DaVinci().HistogramFile = output_name + ".histos.root"
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().appendToMainSequence( [ lc_seq.sequence() ] )

#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]


