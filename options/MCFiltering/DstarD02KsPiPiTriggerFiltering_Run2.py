"""
Turbo Filtering script for Monte Carlo for Hlt2CharmHadD02KsHHTurbo
@author Kamil Fischer, Giulia Tuci
@date   2020-02-13
"""

from Configurables import GaudiSequencer

from PhysSelPython.Wrappers import SelectionSequence
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT2_Code = "HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LLTurbo.*')\
                | HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DDTurbo.*')\
                | HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LL_LTUNBTurbo.*')\
                | HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo.*')\
                | HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurbo.*')\
                | HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurbo.*')"
    )

#
# DaVinci Configuration
#
OutputName = "DSTARD02KsPiPi"
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import DstConf, TurboConf

DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = OutputName+"DVHistos.root"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
DaVinci().RootInTES = '/Event/Turbo/'
if 'Turbo' in DaVinci().properties().keys():
    DaVinci().Turbo = True
else:
    DstConf().Turbo = True
    TurboConf().PersistReco = True

# =============================================================================
# configure the copy-stream algorithm
# =============================================================================
oname = OutputName + '.HLTFILTER.MDST'
from GaudiConf import IOHelper
ioh                = IOHelper       ()
copy               = ioh.outputAlgs (oname, 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters

from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )
