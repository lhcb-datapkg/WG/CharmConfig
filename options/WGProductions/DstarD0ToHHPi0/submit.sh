#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# output/2015/
# lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
#     --name='Charm DstarD0ToHHPi0 2015 MagDown' \
#     --author='cburr' --step='133438' --step='133437' --dq-flag='OK' \
#     --bk-query='/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/CHARM.MDST'
# lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
#     --name='Charm DstarD0ToHHPi0 2015 MagUp' \
#     --author='cburr' --step='133438' --step='133437' --dq-flag='OK' \
#     --bk-query='/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/CHARM.MDST'

# output/2016/
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm DstarD0ToHHPi0 2016 MagDown v2' \
    --author='cburr' --step='133439' --step='133437' --dq-flag='OK' \
    --bk-query='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST'
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm DstarD0ToHHPi0 2016 MagUp v2' \
    --author='cburr' --step='133439' --step='133437' --dq-flag='OK' \
    --bk-query='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST'
