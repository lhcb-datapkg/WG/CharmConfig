OPTIONS_DIR="\$CHARMCONFIGROOT/options/WGProductions/DstarD0ToHHPi0"

run_test \
    "output/2015/" \
    "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/CHARM.MDST" \
    "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/CHARM.MDST" \
    "PiPiPi0_2015.py" "Charm_DstarD0ToHHPi0_DVntuple.root" "133438"

run_test \
    "output/2016/" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST" \
    "PiPiPi0_2016.py" "Charm_DstarD0ToHHPi0_DVntuple.root" "133439"
