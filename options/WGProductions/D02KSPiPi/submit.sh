#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# output/2016/
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm D02KSPiPi 2016 MagDown' \
    --author='cburr' --step='133479' --step='133481' --dq-flag='OK' \
    --inform-email='martha.hilton@cern.ch' \
    --bk-query='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST'
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm D02KSPiPi 2016 MagUp' \
    --author='cburr' --step='133479' --step='133481' --dq-flag='OK' \
    --inform-email='martha.hilton@cern.ch' \
    --bk-query='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST'

# output/2017/
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm D02KSPiPi 2017 MagDown' \
    --author='cburr' --step='133480' --step='133481' --dq-flag='OK' \
    --inform-email='martha.hilton@cern.ch' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST'
lb-run LHCbDIRAC/prod ./create_WG_production.py --create --submit --print \
    --name='Charm D02KSPiPi 2017 MagUp' \
    --author='cburr' --step='133480' --step='133481' --dq-flag='OK' \
    --inform-email='martha.hilton@cern.ch' \
    --bk-query='/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST'
