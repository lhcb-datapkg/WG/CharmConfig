OPTIONS_DIR="\$CHARMCONFIGROOT/options/WGProductions/D02KSPiPi"

run_test \
    "output/2016/" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST" \
    "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST" \
    "D02KSPiPi_2016.py" "Charm_D02KSPiPi_DVntuple.root" "133479"

run_test \
    "output/2017/" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST" \
    "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST" \
    "D02KSPiPi_2017.py" "Charm_D02KSPiPi_DVntuple.root" "133480"
