#########################################################################################################
#########################################################################################################
#########################################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import FilterDesktop
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import TupleToolDecayTreeFitter
from Configurables import GaudiSequencer
from Configurables import CombineParticles
from Configurables import FitDecayTrees
from Configurables import TupleToolDecay
from Configurables import TupleToolTISTOS
from Configurables import L0TriggerTisTos,TriggerTisTos
from Configurables import HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder, ReadHltReport
from Configurables import TisTosParticleTagger
from Configurables import DeterministicPrescaler
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__EvtTupleTool
from Configurables import PrintDecayTree
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand

def FILTER(name,input):
    my_data = AutomaticData(Location=input)
    fltr = Selection("FILTER_"+name,
                     Algorithm = FilterDesktop("Filter_all_"+name,Code='ALL'),
                     RequiredSelections =[my_data])
    seq = SelectionSequence( "SEQ_"+name,
                             TopSelection=fltr )
    return seq

triglist=['L0MuonDecision',
          'L0HadronDecision',
          'Hlt1TrackMuonDecision',
          'Hlt1SingleMuonHighPTDecision', 
          'Hlt1TrackAllL0Decision',
          'Hlt2SingleMuonDecision',
          'Hlt2TopoMu2BodyBBDTDecision',
          'Hlt2TopoMu3BodyBBDTDecision',
          'Hlt2TopoMu4BodyBBDTDecision',
          'Hlt2Topo2BodyBBDTDecision',
          'Hlt2Topo3BodyBBDTDecision',
          'Hlt2Topo4BodyBBDTDecision']

tupletools = []
tupletools.append("TupleToolEventInfo")
tupletools.append("TupleToolPrimaries")

########## COMMON TO ALL MODES #####################
TISTOSTool = TupleToolTISTOS('TISTOSTool')
TISTOSTool.VerboseL0 = True
TISTOSTool.VerboseHlt1 = True
TISTOSTool.VerboseHlt2 = True
TISTOSTool.TriggerList = triglist[:]

#### list of variables for stable particles
LoKi_track=LoKi__Hybrid__TupleTool("LoKi_track")
LoKi_track.Variables =  {
    "ID" : "ID",
    "TRCHI2DOF" : "TRCHI2DOF",
    "GHOSTPROB" : "TRGHOSTPROB",
    "IPCHI2" : "MIPCHI2DV(PRIMARY)",
    "IP" : "MIPDV(PRIMARY)",
    "P"  : "P",
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "PIDpi": "PIDpi",
    "PIDe" : "PIDe",
    "PIDp" : "PIDp",
    "PIDK" : "PIDK",
    "PIDmu" : "PIDmu",
    "IsMuon" : "switch(ISMUON,1,0)",
    "InMuonAcceptance" : "switch(INMUON,1,0)"}

#### list of variables for composite particles
LoKi_D=LoKi__Hybrid__TupleTool("LoKi_D")
LoKi_D.Variables =  {
    "ID" : "ID",
    "P"  : "P",
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "M" : "M",
    "VX" : "VFASPF(VX)",
    "VY" : "VFASPF(VY)",
    "VZ" : "VFASPF(VZ)",
    "VCHI2NDOF" : "VFASPF(VCHI2/VDOF)",
    "IPCHI2" : "MIPCHI2DV(PRIMARY)",
    "IP" : "MIPDV(PRIMARY)" ,
    "DTF_M" : "DTF_FUN ( M , False )",
    "DTF_CHI2NDOF" : "DTF_CHI2NDOF( False )",
    "FD_OWNPV" : "VFASPF(VMINVDDV(PRIMARY))"
    }
LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Variables = ({
    "DTF_DM"      : "DTF_FUN ( CHILD(M,'D0'==ABSID) , False )",
    "DTF_M_CON" : "DTF_FUN ( M , False , 'D0' )",
    "DTF_CHI2NDOF_CON" : "DTF_CHI2NDOF ( False , 'D0' )",
    "DTF_D_ct"      : "DTF_CTAU( 'D0' == ABSID , False )"
    }
                    )


###################################################################
#################### B->D(K-pi+)mu nu X ########################
###################################################################
tuple_kpi = DecayTreeTuple( 'b2D0KpiMuX' )
inputFilter_kpi = FILTER("Kpi","/Event/Semileptonic/Phys/b2D0MuXB2DMuNuXLine/Particles")
tuple_kpi.Inputs = [ inputFilter_kpi.outputLocation() ]
tuple_kpi.ToolList =  tupletools[:]
tuple_kpi.Decay = '[B- -> ^([D0 -> ^K- ^pi+]CC) ^mu-]CC'
tuple_kpi.Branches = {
    "B"   : "[B- -> ([D0 -> K- pi+]CC) mu-]CC",
    "D"   : "[B- -> ^([D0 -> K- pi+]CC) mu-]CC",
    "Mu"  : "[B- -> ([D0 -> K- pi+]CC) ^mu-]CC",
    "K"   : "[B- -> ([D0 -> ^K- pi+]CC) mu-]CC",
    "Pi"  : "[B- -> ([D0 -> K- ^pi+]CC) mu-]CC"
    }

#### for all branches 
for particle in ["B","D","Mu","K","Pi"]:
    tuple_kpi.addTool(TupleToolDecay, name=particle)

####### info for the B 
for composite in [tuple_kpi.B]:
    composite.addTool(LoKi_B)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]    

####### info for the B and D
for composite in [tuple_kpi.B,tuple_kpi.D]:
    composite.addTool(LoKi_D)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D"]

####### info for the daughters
for daug in [tuple_kpi.Mu,tuple_kpi.K, tuple_kpi.Pi]:
    daug.addTool(LoKi_track)
    daug.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_track"]

###### TISTOS
for particle in [tuple_kpi.B,tuple_kpi.D,tuple_kpi.Mu]:
    particle.addTool(TISTOSTool, name="TISTOSTool")    
    particle.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]

####### add TISTOS info to the hadrons (just for Kpi)
for daug in [tuple_kpi.K,tuple_kpi.Pi]:
    daug.addTool(TISTOSTool, name="TISTOSTool")
    daug.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]


###################################################################
#################### B->D(K-pi+)mu nu X SAME-SIGN ########################
###################################################################
tuple_kpiSS = DecayTreeTuple( 'b2D0KpiMuX_SS' )
inputFilter_kpiSS = FILTER("KpiSS","/Event/Semileptonic/Phys/b2D0MuXB2DMuNuXLine/Particles")
tuple_kpiSS.Inputs = [ inputFilter_kpiSS.outputLocation() ]
tuple_kpiSS.ToolList =  tupletools[:]
tuple_kpiSS.Decay = '[B- -> ^(D~0 -> ^K+ ^pi-) ^mu-]CC'
tuple_kpiSS.Branches = {
    "B"   : "[B- -> (D~0 -> K+ pi-) mu-]CC",
    "D"   : "[B- -> ^(D~0 -> K+ pi-) mu-]CC",
    "Mu"  : "[B- -> (D~0 -> K+ pi-) ^mu-]CC",
    "K"   : "[B- -> (D~0 -> ^K+ pi-) mu-]CC",
    "Pi"  : "[B- -> (D~0 -> K+ ^pi-) mu-]CC"
    }

#### for all branches 
for particle in ["B","D","Mu","K","Pi"]:
    tuple_kpiSS.addTool(TupleToolDecay, name=particle)

###### TISTOS
for particle in [tuple_kpiSS.B,tuple_kpiSS.D,tuple_kpiSS.Mu]:
    particle.addTool(TISTOSTool, name="TISTOSTool")    
    particle.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]

####### info for the B 
for composite in [tuple_kpiSS.B]:
    composite.addTool(LoKi_B)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]    

####### info for the B and D
for composite in [tuple_kpiSS.B,tuple_kpiSS.D]:
    composite.addTool(LoKi_D)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D"]

####### info for the daughters
for daug in [tuple_kpiSS.Mu,tuple_kpiSS.K, tuple_kpiSS.Pi]:
    daug.addTool(LoKi_track)
    daug.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_track"]

####### add TISTOS info to the hadrons (just for Kpi)
for daug in [tuple_kpiSS.K,tuple_kpiSS.Pi]:
    daug.addTool(TISTOSTool, name="TISTOSTool")
    daug.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]


###################################################################
#################### B->D(K-K+)mu nu X ########################
###################################################################
tuple_kk = DecayTreeTuple( 'b2D0KKMuX' )
inputFilter_kk = FILTER("KK","/Event/Semileptonic/Phys/b2D0MuXKKB2DMuNuXLine/Particles")
tuple_kk.Inputs = [ inputFilter_kk.outputLocation() ]
tuple_kk.ToolList =  tupletools[:]
tuple_kk.Decay = '[B+ -> ^([D0 -> ^K- ^K+]CC) ^mu+]CC'
tuple_kk.Branches = {
    "B"   : "[B+ -> ([D0 -> K- K+]CC) mu+]CC",
    "D"   : "[B+ -> ^([D0 -> K- K+]CC) mu+]CC",
    "Mu"  : "[B+ -> ([D0 -> K- K+]CC) ^mu+]CC",
    "KM"   : "[B+ -> ([D0 -> ^K- K+]CC) mu+]CC",
    "KP"  : "[B+ -> ([D0 -> K- ^K+]CC) mu+]CC"
    }

#### for all branches 
for particle in ["B","D","Mu","KP","KM"]:
    tuple_kk.addTool(TupleToolDecay, name=particle)

###### TISTOS
for particle in [tuple_kk.B,tuple_kk.D,tuple_kk.Mu]:
    particle.addTool(TISTOSTool, name="TISTOSTool")    
    particle.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]

####### info for the B 
for composite in [tuple_kk.B]:
    composite.addTool(LoKi_B)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]    


####### info for the B and D
for composite in [tuple_kk.B,tuple_kk.D]:
    composite.addTool(LoKi_D)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D"]

####### info for the daughters
for daug in [tuple_kk.Mu,tuple_kk.KP, tuple_kk.KM]:
    daug.addTool(LoKi_track)
    daug.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_track"]

####### add TISTOS info to the hadrons (just for Kk)
for daug in [tuple_kk.KP,tuple_kk.KM]:
    daug.addTool(TISTOSTool, name="TISTOSTool")
    daug.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]


###################################################################
#################### B->D(Pi-Pi+)mu nu X ########################
###################################################################
tuple_pipi = DecayTreeTuple( 'b2D0pipiMuX' )
inputFilter_pipi = FILTER("pipi","/Event/Semileptonic/Phys/b2D0MuXpipiB2DMuNuXLine/Particles")
tuple_pipi.Inputs = [ inputFilter_pipi.outputLocation() ]
tuple_pipi.ToolList =  tupletools[:]
tuple_pipi.Decay = '[B- -> ^([D0 -> ^pi- ^pi+]CC) ^mu-]CC'
tuple_pipi.Branches = {
    "B"   : "[B- -> ([D0 -> pi- pi+]CC) mu-]CC",
    "D"   : "[B- -> ^([D0 -> pi- pi+]CC) mu-]CC",
    "Mu"  : "[B- -> ([D0 -> pi- pi+]CC) ^mu-]CC",
    "PiM"   : "[B- -> ([D0 -> ^pi- pi+]CC) mu-]CC",
    "PiP"  : "[B- -> ([D0 -> pi- ^pi+]CC) mu-]CC"
    }

#### for all branches 
for particle in ["B","D","Mu","PiP","PiM"]:
    tuple_pipi.addTool(TupleToolDecay, name=particle)

###### TISTOS
for particle in [tuple_pipi.B,tuple_pipi.D,tuple_pipi.Mu]:
    particle.addTool(TISTOSTool, name="TISTOSTool")    
    particle.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]

####### info for the B 
for composite in [tuple_pipi.B]:
    composite.addTool(LoKi_B)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]    

####### info for the B and D
for composite in [tuple_pipi.B,tuple_pipi.D]:
    composite.addTool(LoKi_D)
    composite.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D"]

####### info for the daughters
for daug in [tuple_pipi.Mu,tuple_pipi.PiP, tuple_pipi.PiM]:
    daug.addTool(LoKi_track)
    daug.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_track"]

####### add TISTOS info to the hadrons (just for Pipi)
for daug in [tuple_pipi.PiP,tuple_pipi.PiM]:
    daug.addTool(TISTOSTool, name="TISTOSTool")
    daug.ToolList += [ "TupleToolTISTOS/TISTOSTool" ]

########################## COLLECT ALL OF THE NTUPLES ############################
tuples = []
tuples.append(tuple_kpi)
tuples.append(tuple_kpiSS)
tuples.append(tuple_kk)
tuples.append(tuple_pipi)

######################## COMMON TO ALL NTUPLES ###############################
LoKiEVENTVariables = LoKi__Hybrid__EvtTupleTool('LoKiEVENTVariables')
LoKiEVENTVariables.Preambulo = ["from LoKiTracks.decorators import *",
                                "from LoKiNumbers.decorators import *",
                                "from LoKiCore.functions import *"]
LoKiEVENTVariables.VOID_Variables = {
    "nLongTracks"           :  "RECSUMMARY(LHCb.RecSummary.nLongTracks,-1,'/Event/Rec/Summary',False)",
    "nIT"           :  "RECSUMMARY(LHCb.RecSummary.nITClusters,-1,'/Event/Rec/Summary',False)",
    "nOT"           :  "RECSUMMARY(LHCb.RecSummary.nOTClusters,-1,'/Event/Rec/Summary',False)",
    "nPVs"          :  "RECSUMMARY(LHCb.RecSummary.nPVs,-1,'/Event/Rec/Summary',False)"
    }

######### add proper time and event variables
for tuple in tuples:
    tuple.addTool(LoKiEVENTVariables , name = 'LoKiEVENTVariables' )
    tuple.ToolList += ['LoKi::Hybrid::EvtTupleTool/LoKiEVENTVariables']

####### prefilters
from Configurables import LoKi__VoidFilter as Filter
fltr = Filter ( 'Sanity' ,
                Code = " EXISTS ('/Event/Strip/Phys/DecReports')"
                )

from Configurables import LoKi__HDRFilter as StripFilter
stripFilter = StripFilter( 'StripPassFilter',
                           Code = "HLT_PASS('Strippingb2D0MuXB2DMuNuXLineDecision') | "\
                           "HLT_PASS('Strippingb2D0MuXKKB2DMuNuXLineDecision') |" \
                           "HLT_PASS('Strippingb2D0MuXpipiB2DMuNuXLineDecision') |"\
                           "HLT_PASS('Strippingb2D0MuXKpiDCSB2DMuNuXLineDecision') |" \
                           "HLT_PASS('Strippingb2D0MuXDstB2DMuNuXLineDecision') | " \
                           "HLT_PASS('Strippingb2D0MuXKKDstB2DMuNuXLineDecision') | " \
                           "HLT_PASS('Strippingb2D0MuXpipiDstB2DMuNuXLineDecision')" ,
                           Location= "/Event/Strip/Phys/DecReports")

###### Momentum scaling
from Configurables import TrackScaleState 
scaleSeq = GaudiSequencer("scaleSeq")
scaler = TrackScaleState('TrackStateScale')
scaleSeq.Members += [scaler]


###################### DAVINCI SETTINGS ############################################    
DaVinci().appendToMainSequence([scaleSeq])
DaVinci().EventPreFilters = [fltr,stripFilter]
DaVinci().appendToMainSequence([inputFilter_kpi,inputFilter_kpiSS,inputFilter_kk,inputFilter_pipi])
DaVinci().appendToMainSequence(tuples)
DaVinci().TupleFile = "SLYCP_2bodies.root"

from Configurables import  DaVinciInit
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

###################################################################################
####################### THE END ###################################################
###################################################################################
