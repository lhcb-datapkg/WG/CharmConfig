# Author: Andrea Contu
# Date: 14/06/2017
#
# Options to filter S20rXp1 CHARM.MDST stream events according to a logical OR of firing stripping lines
#


from Gaudi.Configuration import *

#LIST OF STRIPPING LINES
lines = ["b2D0MuXKsPiPiLLCharmFromBSemiLine",
         "b2D0MuXKsPiPiDDCharmFromBSemiLine",
         "b2DstarMuXKsPiPiLLCharmFromBSemiLine",
         "b2DstarMuXKsPiPiDDCharmFromBSemiLine",]

#build the filtering string
filterstring="|".join(lines)

#set the stripping filter
from Configurables import LoKi__HDRFilter as StripFilter

_StripFilter = StripFilter( 'StripFilter',
                            Code="HLT_PASS_RE('Stripping("+ filterstring +")Decision')",
                            Location="/Event/Strip/Phys/DecReports" )

from Configurables import DaVinci

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().Simulation = False
DaVinci().EvtMax = -1
DaVinci().EventPreFilters = [_StripFilter]
DaVinci().InputType="MDST"
DaVinci().Lumi=True

# ============================================================================
## IV. Configure uDST writer/copier  
# ============================================================================
## due to tehcnical reasons for production it needs to be done late...

def _configure_output_ () :

    outputfile = 'Charm.mdst'
    ##
    from Gaudi.Configuration import allConfigurables
    fakew  = allConfigurables.get('MyDSTWriter',None)
    if fakew and 'Sel' != fakew.OutputFileSuffix : 
        outputfile = fakew.OutputFileSuffix + '.' + outputfile 

    ##for i in range(1) :
    ##    print 'I AM POST   ACTION!!', outputfile
        
    ##
    from GaudiConf import IOHelper
    ioh    = IOHelper        ( 'ROOT'     , 'ROOT' ) 
    oalgs  = ioh.outputAlgs  ( outputfile , 'InputCopyStream/DSTFilter' )
    
    writer = oalgs[0]
    writer.AcceptAlgs = [ _StripFilter ]
    
    from Configurables import GaudiSequencer
    oseq  = GaudiSequencer ( 'WRITEOUTPUT', Members = oalgs ) 
    
    from Configurables import ApplicationMgr
    AM    = ApplicationMgr ()
    
    if not AM.OutStream :
        AM.OutStream =[]
        
        AM.OutStream.append ( oseq )
        
        
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction ( _configure_output_ )

