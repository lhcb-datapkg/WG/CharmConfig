OPTIONS_DIR="\$CHARMCONFIGROOT/options/WGProductions/RareCharm"

# 2011 Real
run_test \
    "output/2011/" \
    "/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/CHARM.MDST" \
    "/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/CHARM.MDST" \
    "RareCharm4body_Run1.py Data_2011_CHARM.py" "RARECHARM_4BODIES.ROOT" "133320"

