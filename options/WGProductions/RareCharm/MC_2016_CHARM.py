# Helper file to define data type for davinci

year      = '2016'
fileType  = 'MDST'
rootintes = "/Event/AllStreams"

from Configurables import DaVinci, CondDB
dv = DaVinci (  DataType                  = year           ,
                InputType                 = fileType       ,
                RootInTES                 = rootintes      ,
                Simulation                = True,
                Lumi                      = False
             )
db = CondDB  ( LatestGlobalTagByDataType = year )

