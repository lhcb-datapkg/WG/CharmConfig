# Authors: O. Kochebina and B. Viaud
# Date   : 18/11/2011
# changed: D. Mitzel 30/08/2015 
# adapted to stripping21 , Dvinci v46r1p1
# added variables  

############ A few preliminary informations ############


## ATTENTION!!! New variables are added not to everytuple!!!!! CHECK!!


from Gaudi.Configuration import *
MessageSvc().Format = "% F%80W%S%7W%R%T %0W%M"
from Configurables import DaVinci, PrintDecayTree
from Configurables import LoKi__HDRFilter as StripFilter
# get classes to build the SelectionSequence
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence


locationRoot = "/Event/Charm/"    # This depends on input stream!

# Filter the Candidate
from Configurables import FilterDesktop


#DecayTreeTuple
from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger
from Configurables import TupleToolGeometry
from Configurables import TupleToolKinematic
from Configurables import TupleToolPropertime
from Configurables import TupleToolPrimaries
from Configurables import TupleToolEventInfo
from Configurables import TupleToolPid
from Configurables import TupleToolMuonPid
from Configurables import TupleToolRICHPid
from Configurables import TupleToolTrackInfo
from Configurables import TupleToolDecay
from Configurables import TupleToolP2VV
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import TupleToolTrigger
from Configurables import TupleToolTISTOS
from Configurables import TupleToolL0Calo
from Configurables import TupleToolTrackIsolation
from Configurables import TupleToolDecayTreeFitter

# Which one(s) do we run ?

D_KK_tag = True#True 
D_PiPi_tag = True #True#True 
D_KPi_tag = True # True#True 
D_KPi_WS_tag = True#True 

D_K3Pi_tag = True#TrueTrue
D_K3Pi_WS_tag = True#TrueTrue

D_KKPiPi_tag = True#True
D_PiPiPiPi_tag = True#True



############ Few things are common to all ntuples  #####

# Tools in the ntuples
List_of_Tools = [
 "TupleToolEventInfo"
#,"TupleToolTrigger" ###
,"TupleToolEventInfo"
,"TupleToolGeometry"
,"TupleToolKinematic"
,"TupleToolPid"
,"TupleToolRICHPid"
,"TupleToolMuonPid"
,"TupleToolPrimaries"
,"TupleToolPropertime"
,"TupleToolTrackInfo"
#,"TupleToolRecoStats"
,"TupleToolTISTOS"
,"TupleToolL0Calo"
]


# List of variables that do not come by default for D->HHmumu
LoKi_All_hhmumu = LoKi__Hybrid__TupleTool("LoKi_All_hhmumu")
LoKi_All_hhmumu.Variables={
     "DiMuon_Mass" : "M34"
    ,"LV01" : "LV01"
    ,"LV02" : "LV02"
    ,"LV03" : "LV03"
    ,"LV04" : "LV04"
     ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    }


from Configurables import LoKi__Hybrid__TupleTool
LoKi_Cone =  LoKi__Hybrid__TupleTool( 'LoKi_Cone')
LoKi_Cone.Variables = {
  "CONEANGLE_D"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVD1','CONEANGLE',-1.)",
  "CONEMULT_D"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVD1','CONEMULT', -1.)",
  "CONEPTASYM_D"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVD1','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVDstar1','CONEANGLE',-1.)",
  "CONEMULT_Dstar"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVDstar1','CONEMULT',-1.)",
  "CONEPTASYM_Dstar" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuLine/P2CVDstar1','CONEPTASYM',-1.)"
}

# List of variables that do not come by default for D->HHmumu with tag
LoKi_All_hhmumu_Dst = LoKi__Hybrid__TupleTool("LoKi_All_hhmumu_Dst")
LoKi_All_hhmumu_Dst.Variables={
    "DTF_CHI2"            : "DTF_CHI2(True)"
    , "DTF_NDOF"          : "DTF_NDOF(True)"
    , "DTF_Dstarplus_M"   : "DTF_FUN(M, True)"
    , "DTF_Dstarplus_P"   : "DTF_FUN(P, True)"
    , "DTF_Dstarplus_PT"  : "DTF_FUN(PT, True)"
    , "DTF_Dstarplus_E"   : "DTF_FUN(E, True)"
    , "DTF_Dstarplus_PX"  : "DTF_FUN(PX, True)"
    , "DTF_Dstarplus_PY"  : "DTF_FUN(PY, True)"
    , "DTF_Dstarplus_PZ"  : "DTF_FUN(PZ, True)"
    , "DTF_D0_M"          : "DTF_FUN(CHILD(M,1), True)"
    , "DTF_D0_P"          : "DTF_FUN(CHILD(P, 1), True)"
    , "DTF_D0_PT"         : "DTF_FUN(CHILD(PT, 1), True)"
    , "DTF_D0_E"          : "DTF_FUN(CHILD(E, 1), True)"
    , "DTF_D0_PX"         : "DTF_FUN(CHILD(PX, 1), True)"
    , "DTF_D0_PY"         : "DTF_FUN(CHILD(PY, 1), True)"
    , "DTF_D0_PZ"         : "DTF_FUN(CHILD(PZ, 1), True)"
    , "DTF_D0_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 1), True)"
    #, "DTF_D0_CTAU"       : "DTF_CTAU(1,TRUE)"
    , "DTF_Pis_M"         : "DTF_FUN(CHILD(M,2), True)"
    , "DTF_Pis_P"         : "DTF_FUN(CHILD(P, 2), True)"
    , "DTF_Pis_PT"        : "DTF_FUN(CHILD(PT, 2), True)"
    , "DTF_Pis_E"         : "DTF_FUN(CHILD(E, 2), True)"
    , "DTF_Pis_PX"        : "DTF_FUN(CHILD(PX, 2), True)"
    , "DTF_Pis_PY"        : "DTF_FUN(CHILD(PY, 2), True)"
    , "DTF_Pis_PZ"        : "DTF_FUN(CHILD(PZ, 2), True)"
    , "DTF_Pis_BPVIPCHI2" : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 2), True)"
    , "DTF_h0_P"          : "DTF_FUN(CHILD(CHILD(P,1),1),True)"
    , "DTF_h0_PT"         : "DTF_FUN(CHILD(CHILD(PT,1),1),True)"
    , "DTF_h0_E"          : "DTF_FUN(CHILD(CHILD(E,1),1),True)"
    , "DTF_h0_PX"         : "DTF_FUN(CHILD(CHILD(PX,1),1),True)"
    , "DTF_h0_PY"         : "DTF_FUN(CHILD(CHILD(PY,1),1),True)"
    , "DTF_h0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,1),1),True)"
    , "DTF_h1_P"          : "DTF_FUN(CHILD(CHILD(P,2),1),True)"
    , "DTF_h1_PT"         : "DTF_FUN(CHILD(CHILD(PT,2),1),True)"
    , "DTF_h1_E"          : "DTF_FUN(CHILD(CHILD(E,2),1),True)"
    , "DTF_h1_PX"         : "DTF_FUN(CHILD(CHILD(PX,2),1),True)"
    , "DTF_h1_PY"         : "DTF_FUN(CHILD(CHILD(PY,2),1),True)"
    , "DTF_h1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,2),1),True)"
    , "DTF_mu0_P"          : "DTF_FUN(CHILD(CHILD(P,3),1),True)"
    , "DTF_mu0_PT"         : "DTF_FUN(CHILD(CHILD(PT,3),1),True)"
    , "DTF_mu0_E"          : "DTF_FUN(CHILD(CHILD(E,3),1),True)"
    , "DTF_mu0_PX"         : "DTF_FUN(CHILD(CHILD(PX,3),1),True)"
    , "DTF_mu0_PY"         : "DTF_FUN(CHILD(CHILD(PY,3),1),True)"
    , "DTF_mu0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,3),1),True)"
    , "DTF_mu1_P"          : "DTF_FUN(CHILD(CHILD(P,4),1),True)"
    , "DTF_mu1_PT"         : "DTF_FUN(CHILD(CHILD(PT,4),1),True)"
    , "DTF_mu1_E"          : "DTF_FUN(CHILD(CHILD(E,4),1),True)"
    , "DTF_mu1_PX"         : "DTF_FUN(CHILD(CHILD(PX,4),1),True)"
    , "DTF_mu1_PY"         : "DTF_FUN(CHILD(CHILD(PY,4),1),True)"
    , "DTF_mu1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,4),1),True)"
    ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    }


# Prerequisites to get the trigger info

tttt = TupleToolTISTOS()



l0TriggerLines=[ "L0HadronDecision"
                ,"L0MuonDecision"
                ,"L0DiMuonDecision"
                ,"L0ElectronDecision"
                ,"L0PhotonDecision"]


hlt1TriggerLines=['Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1SingleMuonNoIPDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1TrackAllL0Decision', 'Hlt1TrackMuonDecision', 'Hlt1TrackPhotonDecision', 'Hlt1L0AnyDecision', 'Hlt1GlobalDecision']


hlt2TriggerLines=['Hlt2SingleMuonDecision','Hlt2DiMuonDetachedDecision', 'Hlt2CharmSemilepD2HMuMuDecision', 'Hlt2DiMuonDetachedDecision','Hlt2CharmSemilep3bodyD2PiMuMuDecision', 'Hlt2CharmSemilep3bodyD2KMuMuDecision', 'Hlt2CharmSemilep3bodyD2PiMuMuSSDecision', 'Hlt2CharmSemilep3bodyD2KMuMuSSDecision', 'Hlt2CharmSemilepD02PiPiMuMuDecision', 'Hlt2CharmSemilepD02KKMuMuDecision','Hlt2CharmSemilepD02KPiMuMuDecision','Hlt2CharmHadD02HHHHDst_4piDecision','Hlt2CharmHadD02HHHHDst_K3piDecision','Hlt2CharmHadD02HHHHDst_KKpipiDecision','Hlt2CharmHadD02HHXDst_hhXDecision', 'Hlt2CharmHadD02HHXDst_hhXWideMassDecision', 'Hlt2CharmHadD02HHXDst_BaryonhhXDecision', 'Hlt2CharmHadD02HHXDst_BaryonhhXWideMassDecision','Hlt2CharmHadD02HHXDst_LeptonhhXDecision', 'Hlt2CharmHadD02HHXDst_LeptonhhXWideMassDecision','Hlt2CharmHadD02HHHH_K3piDecision', 'Hlt2CharmHadD02HHHH_K3piWideMassDecision','Hlt2CharmHadD02HHHH_KKpipiDecision', 'Hlt2CharmHadD02HHHH_KKpipiWideMassDecision','Hlt2CharmHadD02HHHH_4piDecision', 'Hlt2CharmHadD02HHHH_4piWideMassDecision','Hlt2CharmSemilepD02HHMuMuDecision']

triggerLines=l0TriggerLines+hlt1TriggerLines+hlt2TriggerLines



TTTI = TupleToolTrackIsolation("TTTI")
tos=TupleToolTISTOS("tos")


###################################################################
###        4-body decays with TAG
###    
####################################################################

############ Creation of the D*-> D->KKmumu ntuple #######################
# Def
Tuple16 = DecayTreeTuple("DstD2KKMuMu")
Tuple16.RootInTES      = locationRoot

# Trigger
Tuple16.addTool(tttt)

# Get KKmumu candidates and start the ntuple
Tuple16.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuLine' ]
Tuple16.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^K- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple16.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ K- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^K- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ K- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ K- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ K- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                }


Tuple16.ToolList = List_of_Tools

# Additional variables, that do not come by default
Tuple16.addTool(TupleToolDecay, name="D")
Tuple16.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple16.D.addTool(LoKi_All_hhmumu)

# TISTOS
#Tuple16.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple16.TupleToolTISTOS.Verbose=True
#Tuple16.TupleToolTISTOS.TriggerList = triggerLines


####
#Tuple16.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_D"]
#Tuple16.D.addTool(LoKi_Cone_D)


Tuple16.D.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple16.D.addTool(TTTI)
Tuple16.D.TTTI.FillAsymmetry=True
Tuple16.D.TTTI.FillDeltaAngles=True
Tuple16.D.TTTI.MinConeAngle=1.0
Tuple16.D.TTTI.MaxConeAngle=1.8

Tuple16.addTool(TupleToolPid,name="TupleToolPid")
Tuple16.TupleToolPid.Verbose = True

Tuple16.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple16.TupleToolGeometry.Verbose = True

Tuple16.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple16.TupleToolTrackInfo.Verbose = True


Tuple16.addTool(TupleToolDecay, name="Dst")
Tuple16.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple16.Dst.addTool(LoKi_All_hhmumu_Dst)

# Isolaiton for D*                                                                                                                                                                   
####                                                                                                                                                                                
#Tuple16.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]                                                                                                                     
#Tuple16.addTool(LoKi_Cone)  

Tuple16.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple16.Dst.addTool(LoKi_Cone)
         
Tuple16.Dst.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple16.Dst.addTool(TTTI)
Tuple16.Dst.TTTI.FillAsymmetry=True
Tuple16.Dst.TTTI.FillDeltaAngles=True
Tuple16.Dst.TTTI.MinConeAngle=1.0
Tuple16.Dst.TTTI.MaxConeAngle=1.8


##Trigger
Tuple16.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.D.addTool(tos)
Tuple16.D.tos.VerboseL0=True
Tuple16.D.tos.VerboseHlt1=True
Tuple16.D.tos.VerboseHlt2=True
Tuple16.D.tos.TriggerList = triggerLines

Tuple16.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.Dst.addTool(tos)
Tuple16.Dst.tos.VerboseL0=True
Tuple16.Dst.tos.VerboseHlt1=True
Tuple16.Dst.tos.VerboseHlt2=True
Tuple16.Dst.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="h0")
Tuple16.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.h0.addTool(tos)
Tuple16.h0.tos.VerboseL0=True
Tuple16.h0.tos.VerboseHlt1=True
Tuple16.h0.tos.VerboseHlt2=True
Tuple16.h0.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="h1")
Tuple16.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.h1.addTool(tos)
Tuple16.h1.tos.VerboseL0=True
Tuple16.h1.tos.VerboseHlt1=True
Tuple16.h1.tos.VerboseHlt2=True
Tuple16.h1.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="mu0")
Tuple16.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.mu0.addTool(tos)
Tuple16.mu0.tos.VerboseL0=True
Tuple16.mu0.tos.VerboseHlt1=True
Tuple16.mu0.tos.VerboseHlt2=True
Tuple16.mu0.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolDecay, name="mu1")
Tuple16.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple16.mu1.addTool(tos)
Tuple16.mu1.tos.VerboseL0=True
Tuple16.mu1.tos.VerboseHlt1=True
Tuple16.mu1.tos.VerboseHlt2=True
Tuple16.mu1.tos.TriggerList = triggerLines

Tuple16.addTool(TupleToolL0Calo())
Tuple16.TupleToolL0Calo.WhichCalo = "HCAL"

############ Creation of the D*-> D->PiPimumu ntuple #######################
# Def
Tuple17 = DecayTreeTuple("DstD2PiPiMuMu")
Tuple17.RootInTES      = locationRoot

# Trigger
Tuple17.addTool(tttt)


# Get KKmumu candidates and start the ntuple
Tuple17.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuLine' ]
Tuple17.Decay =  "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple17.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> pi+ ^pi- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                }


Tuple17.ToolList = List_of_Tools

# Additional variables, that do not come by default
Tuple17.addTool(TupleToolDecay, name="D")
Tuple17.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple17.D.addTool(LoKi_All_hhmumu)

# TISTOS
#Tuple17.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple17.TupleToolTISTOS.Verbose=True
#Tuple17.TupleToolTISTOS.TriggerList = triggerLines

#Tuple17.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
#Tuple17.D.addTool(LoKi_Cone)

Tuple17.D.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple17.D.addTool(TTTI)
Tuple17.D.TTTI.FillAsymmetry=True
Tuple17.D.TTTI.FillDeltaAngles=True
Tuple17.D.TTTI.MinConeAngle=1.0
Tuple17.D.TTTI.MaxConeAngle=1.8

Tuple17.addTool(TupleToolPid,name="TupleToolPid")
Tuple17.TupleToolPid.Verbose = True

Tuple17.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple17.TupleToolGeometry.Verbose = True

Tuple17.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple17.TupleToolTrackInfo.Verbose = True


# Additional variables, that do not come by default
Tuple17.addTool(TupleToolDecay, name="Dst")
Tuple17.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple17.Dst.addTool(LoKi_All_hhmumu_Dst)

Tuple17.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple17.Dst.addTool(LoKi_Cone)

Tuple17.Dst.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple17.Dst.addTool(TTTI)
Tuple17.Dst.TTTI.FillAsymmetry=True
Tuple17.Dst.TTTI.FillDeltaAngles=True
Tuple17.Dst.TTTI.MinConeAngle=1.0
Tuple17.Dst.TTTI.MaxConeAngle=1.8


##Trigger
Tuple17.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.D.addTool(tos)
Tuple17.D.tos.VerboseL0=True
Tuple17.D.tos.VerboseHlt1=True
Tuple17.D.tos.VerboseHlt2=True
Tuple17.D.tos.TriggerList = triggerLines

Tuple17.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.Dst.addTool(tos)
Tuple17.Dst.tos.VerboseL0=True
Tuple17.Dst.tos.VerboseHlt1=True
Tuple17.Dst.tos.VerboseHlt2=True
Tuple17.Dst.tos.TriggerList = triggerLines

Tuple17.addTool(TupleToolDecay, name="h0")
Tuple17.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.h0.addTool(tos)
Tuple17.h0.tos.VerboseL0=True
Tuple17.h0.tos.VerboseHlt1=True
Tuple17.h0.tos.VerboseHlt2=True
Tuple17.h0.tos.TriggerList = triggerLines

Tuple17.addTool(TupleToolDecay, name="h1")
Tuple17.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.h1.addTool(tos)
Tuple17.h1.tos.VerboseL0=True
Tuple17.h1.tos.VerboseHlt1=True
Tuple17.h1.tos.VerboseHlt2=True
Tuple17.h1.tos.TriggerList = triggerLines

Tuple17.addTool(TupleToolDecay, name="mu0")
Tuple17.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.mu0.addTool(tos)
Tuple17.mu0.tos.VerboseL0=True
Tuple17.mu0.tos.VerboseHlt1=True
Tuple17.mu0.tos.VerboseHlt2=True
Tuple17.mu0.tos.TriggerList = triggerLines

Tuple17.addTool(TupleToolDecay, name="mu1")
Tuple17.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple17.mu1.addTool(tos)
Tuple17.mu1.tos.VerboseL0=True
Tuple17.mu1.tos.VerboseHlt1=True
Tuple17.mu1.tos.VerboseHlt2=True
Tuple17.mu1.tos.TriggerList = triggerLines

Tuple17.addTool(TupleToolL0Calo())
Tuple17.TupleToolL0Calo.WhichCalo = "HCAL"
############ Creation of the D*-> D->K-Pi+mumu ntuple #######################
# Def
Tuple18 = DecayTreeTuple("DstD2KPiMuMu")
Tuple18.RootInTES      = locationRoot
 
# Trigger
Tuple18.addTool(tttt)


# Get KKmumu candidates and start the ntuple
Tuple18.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuLine' ]
Tuple18.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^mu- ) ^pi+ ]CC"
Tuple18.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                }


Tuple18.ToolList = List_of_Tools

# Additional variables, that do not come by default
Tuple18.addTool(TupleToolDecay, name="D")
Tuple18.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple18.D.addTool(LoKi_All_hhmumu)

# TISTOS
#Tuple18.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple18.TupleToolTISTOS.Verbose=True
#Tuple18.TupleToolTISTOS.TriggerList = triggerLines

#Tuple18.addTool(TupleToolDecay, name="D")

Tuple18.D.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple18.D.addTool(TTTI)
Tuple18.D.TTTI.FillAsymmetry=True
Tuple18.D.TTTI.FillDeltaAngles=True
Tuple18.D.TTTI.MinConeAngle=1.0
Tuple18.D.TTTI.MaxConeAngle=1.8

Tuple18.addTool(TupleToolPid,name="TupleToolPid")
Tuple18.TupleToolPid.Verbose = True

Tuple18.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple18.TupleToolGeometry.Verbose = True

Tuple18.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple18.TupleToolTrackInfo.Verbose = True


# Additional variables, that do not come by default
Tuple18.addTool(TupleToolDecay, name="Dst")
Tuple18.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple18.Dst.addTool(LoKi_All_hhmumu_Dst)

Tuple18.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple18.Dst.addTool(LoKi_Cone)

Tuple18.Dst.ToolList += ["TupleToolTrackIsolation/TTTI"]
Tuple18.Dst.addTool(TTTI)
Tuple18.Dst.TTTI.FillAsymmetry=True
Tuple18.Dst.TTTI.FillDeltaAngles=True
Tuple18.Dst.TTTI.MinConeAngle=1.0
Tuple18.Dst.TTTI.MaxConeAngle=1.8

#Tuple18.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
#Tuple18.Dst.addTool(LoKi_Cone)  

##Trigger
Tuple18.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.D.addTool(tos)
Tuple18.D.tos.VerboseL0=True
Tuple18.D.tos.VerboseHlt1=True
Tuple18.D.tos.VerboseHlt2=True
Tuple18.D.tos.TriggerList = triggerLines

Tuple18.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.Dst.addTool(tos)
Tuple18.Dst.tos.VerboseL0=True
Tuple18.Dst.tos.VerboseHlt1=True
Tuple18.Dst.tos.VerboseHlt2=True
Tuple18.Dst.tos.TriggerList = triggerLines

Tuple18.addTool(TupleToolDecay, name="h0")
Tuple18.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.h0.addTool(tos)
Tuple18.h0.tos.VerboseL0=True
Tuple18.h0.tos.VerboseHlt1=True
Tuple18.h0.tos.VerboseHlt2=True
Tuple18.h0.tos.TriggerList = triggerLines

Tuple18.addTool(TupleToolDecay, name="h1")
Tuple18.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.h1.addTool(tos)
Tuple18.h1.tos.VerboseL0=True
Tuple18.h1.tos.VerboseHlt1=True
Tuple18.h1.tos.VerboseHlt2=True
Tuple18.h1.tos.TriggerList = triggerLines

Tuple18.addTool(TupleToolDecay, name="mu0")
Tuple18.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.mu0.addTool(tos)
Tuple18.mu0.tos.VerboseL0=True
Tuple18.mu0.tos.VerboseHlt1=True
Tuple18.mu0.tos.VerboseHlt2=True
Tuple18.mu0.tos.TriggerList = triggerLines

Tuple18.addTool(TupleToolDecay, name="mu1")
Tuple18.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple18.mu1.addTool(tos)
Tuple18.mu1.tos.VerboseL0=True
Tuple18.mu1.tos.VerboseHlt1=True
Tuple18.mu1.tos.VerboseHlt2=True
Tuple18.mu1.tos.TriggerList = triggerLines

Tuple18.addTool(TupleToolL0Calo())
Tuple18.TupleToolL0Calo.WhichCalo = "HCAL"
############ Creation of the D*-> D->K+Pi-mumu WS ntuple #######################
# Def
Tuple19 = DecayTreeTuple("DstD2KPiMuMuWS")
Tuple19.RootInTES      = locationRoot

# Trigger
Tuple19.addTool(tttt)


# Get KKmumu candidates and start the ntuple
Tuple19.Inputs = [ 'Phys/DstarPromptWithD02HHMuMuLine' ]
Tuple19.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC"
Tuple19.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- mu+ mu- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^pi- mu+ mu- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^mu+ mu- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ ^mu- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
                }


Tuple19.ToolList = List_of_Tools

# Additional variables, that do not come by default
Tuple19.addTool(TupleToolDecay, name="D")
Tuple19.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu"]
Tuple19.D.addTool(LoKi_All_hhmumu)

# TISTOS
#Tuple19.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple19.TupleToolTISTOS.Verbose=True
#Tuple19.TupleToolTISTOS.TriggerList = triggerLines

# Additional variables, that do not come by default
Tuple19.addTool(TupleToolDecay, name="Dst")
Tuple19.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_hhmumu_Dst"]
Tuple19.Dst.addTool(LoKi_All_hhmumu_Dst)

Tuple19.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone"]
Tuple19.Dst.addTool(LoKi_Cone)



##Trigger
Tuple19.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.D.addTool(tos)
Tuple19.D.tos.VerboseL0=True
Tuple19.D.tos.VerboseHlt1=True
Tuple19.D.tos.VerboseHlt2=True
Tuple19.D.tos.TriggerList = triggerLines


Tuple19.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.Dst.addTool(tos)
Tuple19.Dst.tos.VerboseL0=True
Tuple19.Dst.tos.VerboseHlt1=True
Tuple19.Dst.tos.VerboseHlt2=True
Tuple19.Dst.tos.TriggerList = triggerLines

Tuple19.addTool(TupleToolDecay, name="h0")
Tuple19.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.h0.addTool(tos)
Tuple19.h0.tos.VerboseL0=True
Tuple19.h0.tos.VerboseHlt1=True
Tuple19.h0.tos.VerboseHlt2=True
Tuple19.h0.tos.TriggerList = triggerLines

Tuple19.addTool(TupleToolDecay, name="h1")
Tuple19.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.h1.addTool(tos)
Tuple19.h1.tos.VerboseL0=True
Tuple19.h1.tos.VerboseHlt1=True
Tuple19.h1.tos.VerboseHlt2=True
Tuple19.h1.tos.TriggerList = triggerLines

Tuple19.addTool(TupleToolDecay, name="mu0")
Tuple19.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.mu0.addTool(tos)
Tuple19.mu0.tos.VerboseL0=True
Tuple19.mu0.tos.VerboseHlt1=True
Tuple19.mu0.tos.VerboseHlt2=True
Tuple19.mu0.tos.TriggerList = triggerLines

Tuple19.addTool(TupleToolDecay, name="mu1")
Tuple19.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple19.mu1.addTool(tos)
Tuple19.mu1.tos.VerboseL0=True
Tuple19.mu1.tos.VerboseHlt1=True
Tuple19.mu1.tos.VerboseHlt2=True
Tuple19.mu1.tos.TriggerList = triggerLines

Tuple19.addTool(TupleToolL0Calo())
Tuple19.TupleToolL0Calo.WhichCalo = "HCAL"
####### Get the GEC variables #############################################

from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool("LoKi_EvtTuple")
LoKi_EvtTuple.VOID_Variables = {
    # track information
    "nLong"       : "RECSUMMARY(LHCb.RecSummary.nLongTracks      , -999, '', False )"
    ,"nUpstream"   : "RECSUMMARY(LHCb.RecSummary.nUpstreamTracks  , -999, '', False )"
    ,"nDownstream" : "RECSUMMARY(LHCb.RecSummary.nDownstreamTracks, -999, '', False )"
    ,"nBackward"   : "RECSUMMARY(LHCb.RecSummary.nBackTracks      , -999, '', False )" 
    ,"nMuon"       : "RECSUMMARY(LHCb.RecSummary.nMuonTracks      , -999, '', False )"
    ,"nVELO"       : "RECSUMMARY(LHCb.RecSummary.nVeloTracks      , -999, '', False )"
    ,"nTracks"         : "RECSUMMARY( LHCb.RecSummary.nTracks,-1,'/Event/Rec/Summary',False )"
     # pileup
    ,"nPVs"        : "RECSUMMARY(LHCb.RecSummary.nPVs, -999, '', False )"
     
     # tracking multiplicities
    ,"nSpdDigits"  : "RECSUMMARY(LHCb.RecSummary.nSPDhits,    -999, '', False )"
    ,"nITClusters" : "RECSUMMARY(LHCb.RecSummary.nITClusters, -999, '', False )"
    ,"nTTClusters" : "RECSUMMARY(LHCb.RecSummary.nTTClusters, -999, '', False )"
    }

LoKi_EvtTuple.Preambulo +=['from LoKiTracks.decorators import *',
                           'from LoKiNumbers.decorators import *',
                           'from LoKiCore.functions  import *' ]
    
# Put the GECs in the ntuples !!


Tuple16.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
Tuple16.addTool(LoKi_EvtTuple)

Tuple17.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
Tuple17.addTool(LoKi_EvtTuple)

Tuple18.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
Tuple18.addTool(LoKi_EvtTuple)

Tuple19.ToolList+=["LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple"]
Tuple19.addTool(LoKi_EvtTuple)


############ Execution part (ie Sequences and DV settings) #####
from Configurables import GaudiSequencer


Seq_KK_tag = GaudiSequencer("Seq_KK_tag")
Seq_KK_tag.RootInTES = locationRoot
Seq_KK_tag.Members  += [ Tuple16 ]

Seq_PiPi_tag = GaudiSequencer("Seq_PiPi_tag")
Seq_PiPi_tag.RootInTES = locationRoot
Seq_PiPi_tag.Members  += [ Tuple17 ]

Seq_KPi_tag = GaudiSequencer("Seq_KPi_tag")
Seq_KPi_tag.RootInTES = locationRoot
Seq_KPi_tag.Members  += [ Tuple18 ]

Seq_KPi_WS_tag = GaudiSequencer("Seq_KPi_WS_tag")
Seq_KPi_WS_tag.RootInTES = locationRoot
Seq_KPi_WS_tag.Members  += [ Tuple19 ]


# List of variables that do not come by default for D->HHHH (4h)
LoKi_All_4h = LoKi__Hybrid__TupleTool("LoKi_All_4h")
LoKi_All_4h.Variables={
     "DiMuon_Mass" : "M34"
    ,"LV01" : "LV01"
    ,"LV02" : "LV02"
    ,"LV03" : "LV03"
    ,"LV04" : "LV04"
    ,"DTF_CHI2"            : "DTF_CHI2(True)"
    ,"DTF_NDOF"          : "DTF_NDOF(True)"
    ,"D_DTF_M"    : "DTF_FUN ( M , True)"
    ,"D_DTF_M_False" : "DTF_FUN ( M , False)"
    ,"h0_DTF_PX"  : "DTF_FUN (CHILD(1,PX),True)"
    ,"h1_DTF_PX"  : "DTF_FUN (CHILD(2,PX),True)"
    ,"mu0_DTF_PX" : "DTF_FUN (CHILD(3,PX),True)"
    ,"mu1_DTF_PX" : "DTF_FUN (CHILD(4,PX),True)"
    ,"h0_DTF_PY"  : "DTF_FUN (CHILD(1,PY),True)"
    ,"h1_DTF_PY"  : "DTF_FUN (CHILD(2,PY),True)"
    ,"mu0_DTF_PY" : "DTF_FUN (CHILD(3,PY),True)"
    ,"mu1_DTF_PY" : "DTF_FUN (CHILD(4,PY),True)"
    ,"h0_DTF_PZ"  : "DTF_FUN (CHILD(1,PZ),True)"
    ,"h1_DTF_PZ"  : "DTF_FUN (CHILD(2,PZ),True)"
    ,"mu0_DTF_PZ" : "DTF_FUN (CHILD(3,PZ),True)"
    ,"mu1_DTF_PZ" : "DTF_FUN (CHILD(4,PZ),True)"
    ,"h0_DTF_P"  : "DTF_FUN (CHILD(1,P),True)"
    ,"h1_DTF_P"  : "DTF_FUN (CHILD(2,P),True)"
    ,"mu0_DTF_P"  : "DTF_FUN (CHILD(3,P),True)"
    ,"mu1_DTF_P"  : "DTF_FUN (CHILD(4,P),True)"
    ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    }


# List of variables that do not come by default for D->HHHH (4h) with tag
LoKi_All_4h_Dst = LoKi__Hybrid__TupleTool("LoKi_All_4h_Dst")
LoKi_All_4h_Dst.Variables={
    "DTF_CHI2"            : "DTF_CHI2(True)"
    , "DTF_NDOF"          : "DTF_NDOF(True)"
    , "DTF_Dstarplus_M"   : "DTF_FUN(M, True)"
    , "DTF_Dstarplus_P"   : "DTF_FUN(P, True)"
    , "DTF_Dstarplus_PT"  : "DTF_FUN(PT, True)"
    , "DTF_Dstarplus_E"   : "DTF_FUN(E, True)"
    , "DTF_Dstarplus_PX"  : "DTF_FUN(PX, True)"
    , "DTF_Dstarplus_PY"  : "DTF_FUN(PY, True)"
    , "DTF_Dstarplus_PZ"  : "DTF_FUN(PZ, True)"
    , "DTF_D0_M"          : "DTF_FUN(CHILD(M,1), True)"
    , "DTF_D0_P"          : "DTF_FUN(CHILD(P, 1), True)"
    , "DTF_D0_PT"         : "DTF_FUN(CHILD(PT, 1), True)"
    , "DTF_D0_E"          : "DTF_FUN(CHILD(E, 1), True)"
    , "DTF_D0_PX"         : "DTF_FUN(CHILD(PX, 1), True)"
    , "DTF_D0_PY"         : "DTF_FUN(CHILD(PY, 1), True)"
    , "DTF_D0_PZ"         : "DTF_FUN(CHILD(PZ, 1), True)"
    , "DTF_D0_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 1), True)"
    #, "DTF_D0_CTAU"       : "DTF_CTAU(1,TRUE)"                                                                                                                                             
    , "DTF_Pis_M"         : "DTF_FUN(CHILD(M,2), True)"
    , "DTF_Pis_P"         : "DTF_FUN(CHILD(P, 2), True)"
    , "DTF_Pis_PT"        : "DTF_FUN(CHILD(PT, 2), True)"
    , "DTF_Pis_E"         : "DTF_FUN(CHILD(E, 2), True)"
    , "DTF_Pis_PX"        : "DTF_FUN(CHILD(PX, 2), True)"
    , "DTF_Pis_PY"        : "DTF_FUN(CHILD(PY, 2), True)"
    , "DTF_Pis_PZ"        : "DTF_FUN(CHILD(PZ, 2), True)"
    , "DTF_Pis_BPVIPCHI2" : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 2), True)"
    , "DTF_h0_P"          : "DTF_FUN(CHILD(CHILD(P,1),1),True)"
    , "DTF_h0_PT"         : "DTF_FUN(CHILD(CHILD(PT,1),1),True)"
    , "DTF_h0_E"          : "DTF_FUN(CHILD(CHILD(E,1),1),True)"
    , "DTF_h0_PX"         : "DTF_FUN(CHILD(CHILD(PX,1),1),True)"
    , "DTF_h0_PY"         : "DTF_FUN(CHILD(CHILD(PY,1),1),True)"
    , "DTF_h0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,1),1),True)"
    , "DTF_h1_P"          : "DTF_FUN(CHILD(CHILD(P,2),1),True)"
    , "DTF_h1_PT"         : "DTF_FUN(CHILD(CHILD(PT,2),1),True)"
    , "DTF_h1_E"          : "DTF_FUN(CHILD(CHILD(E,2),1),True)"
    , "DTF_h1_PX"         : "DTF_FUN(CHILD(CHILD(PX,2),1),True)"
    , "DTF_h1_PY"         : "DTF_FUN(CHILD(CHILD(PY,2),1),True)"
    , "DTF_h1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,2),1),True)"
    , "DTF_mu0_P"          : "DTF_FUN(CHILD(CHILD(P,3),1),True)"
    , "DTF_mu0_PT"         : "DTF_FUN(CHILD(CHILD(PT,3),1),True)"
    , "DTF_mu0_E"          : "DTF_FUN(CHILD(CHILD(E,3),1),True)"
    , "DTF_mu0_PX"         : "DTF_FUN(CHILD(CHILD(PX,3),1),True)"
    , "DTF_mu0_PY"         : "DTF_FUN(CHILD(CHILD(PY,3),1),True)"
    , "DTF_mu0_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,3),1),True)"
    , "DTF_mu1_P"          : "DTF_FUN(CHILD(CHILD(P,4),1),True)"
    , "DTF_mu1_PT"         : "DTF_FUN(CHILD(CHILD(PT,4),1),True)"
    , "DTF_mu1_E"          : "DTF_FUN(CHILD(CHILD(E,4),1),True)"
    , "DTF_mu1_PX"         : "DTF_FUN(CHILD(CHILD(PX,4),1),True)"
    , "DTF_mu1_PY"         : "DTF_FUN(CHILD(CHILD(PY,4),1),True)"
    , "DTF_mu1_PZ"         : "DTF_FUN(CHILD(CHILD(PZ,4),1),True)"
    ,"MAXDOCA" : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
    }

LoKi_Cone_4h_Dst=  LoKi__Hybrid__TupleTool( 'LoKi_Cone_4h_Dst')
LoKi_Cone_4h_Dst.Variables = {
  "CONEANGLE_D"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVD1','CONEANGLE',-1.)",
  "CONEMULT_D"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVD1','CONEMULT', -1.)",
  "CONEPTASYM_D"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVD1','CONEPTASYM',-1.)",
  "CONEANGLE_Dstar"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVDstar1','CONEANGLE',-1.)",
  "CONEMULT_Dstar"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVDstar1','CONEMULT',-1.)",
  "CONEPTASYM_Dstar" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHHHLine/P2CVDstar1','CONEPTASYM',-1.)"
}

############ Creation of the D*-> D->K-Pi+Pi+Pi- ntuple #######################
# Def

Tuple20 = DecayTreeTuple("DstD2KPiPiPi")
Tuple20.RootInTES      = locationRoot 

# Trigger
Tuple20.addTool(tttt)


# Get KKpipi candidates and start the ntuple
#Tuple20.Inputs = [ 'Phys/DstarPromptWithD02HHHHNoPIDLine' ]
Tuple20.Inputs = [ 'Phys/DstarPromptWithD02HHHHLine' ]                                                                                                                                
Tuple20.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+ ^pi- ) ^pi+ ]CC"
Tuple20.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ pi+ pi- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K- ^pi+ pi+ pi- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^pi+ pi- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ ^pi- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ pi- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K- pi+ pi+ pi- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ pi- ) pi+ ]CC"
                }


Tuple20.ToolList = List_of_Tools


# Additional variables, that do not come by default                                                                                                                                         
Tuple20.addTool(TupleToolDecay, name="Dst")
Tuple20.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h_Dst"]
Tuple20.Dst.addTool(LoKi_All_4h_Dst)

Tuple20.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_4h_Dst"]
Tuple20.Dst.addTool(LoKi_Cone_4h_Dst)

# Additional variables, that do not come by default
Tuple20.addTool(TupleToolDecay, name="D")
Tuple20.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h"]
Tuple20.D.addTool(LoKi_All_4h)

Tuple20.addTool(TupleToolPid,name="TupleToolPid")
Tuple20.TupleToolPid.Verbose = True

Tuple20.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple20.TupleToolGeometry.Verbose = True

Tuple20.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple20.TupleToolTrackInfo.Verbose = True

# TISTOS
#Tuple20.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple20.TupleToolTISTOS.Verbose=True
#Tuple20.TupleToolTISTOS.TriggerList = triggerLines

##Trigger
Tuple20.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.D.addTool(tos)
Tuple20.D.tos.VerboseL0=True
Tuple20.D.tos.VerboseHlt1=True
Tuple20.D.tos.VerboseHlt2=True
Tuple20.D.tos.TriggerList = triggerLines

Tuple20.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.Dst.addTool(tos)
Tuple20.Dst.tos.VerboseL0=True
Tuple20.Dst.tos.VerboseHlt1=True
Tuple20.Dst.tos.VerboseHlt2=True
Tuple20.Dst.tos.TriggerList = triggerLines

Tuple20.addTool(TupleToolDecay, name="h0")
Tuple20.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.h0.addTool(tos)
Tuple20.h0.tos.VerboseL0=True
Tuple20.h0.tos.VerboseHlt1=True
Tuple20.h0.tos.VerboseHlt2=True
Tuple20.h0.tos.TriggerList = triggerLines

Tuple20.addTool(TupleToolDecay, name="h1")
Tuple20.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.h1.addTool(tos)
Tuple20.h1.tos.VerboseL0=True
Tuple20.h1.tos.VerboseHlt1=True
Tuple20.h1.tos.VerboseHlt2=True
Tuple20.h1.tos.TriggerList = triggerLines

Tuple20.addTool(TupleToolDecay, name="mu0")
Tuple20.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.mu0.addTool(tos)
Tuple20.mu0.tos.VerboseL0=True
Tuple20.mu0.tos.VerboseHlt1=True
Tuple20.mu0.tos.VerboseHlt2=True
Tuple20.mu0.tos.TriggerList = triggerLines

Tuple20.addTool(TupleToolDecay, name="mu1")
Tuple20.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple20.mu1.addTool(tos)
Tuple20.mu1.tos.VerboseL0=True
Tuple20.mu1.tos.VerboseHlt1=True
Tuple20.mu1.tos.VerboseHlt2=True
Tuple20.mu1.tos.TriggerList = triggerLines

Tuple20.addTool(TupleToolL0Calo())
Tuple20.TupleToolL0Calo.WhichCalo = "HCAL"
############ Creation of the D*-> D->K+Pi-Pi-Pi+ ntuple #######################
# Def
Tuple21 = DecayTreeTuple("DstD2KPiPiPiWS")
Tuple21.RootInTES      = locationRoot

# Trigger
Tuple21.addTool(tttt)


# Get KKmumu candidates and start the ntuple
#Tuple21.Inputs = [ 'Phys/DstarPromptWithD02HHHHNoPIDLine' ]
Tuple21.Inputs = [ 'Phys/DstarPromptWithD02HHHHLine' ]
Tuple21.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^pi- ^pi+ ) ^pi+ ]CC"
Tuple21.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- pi- pi+ ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^pi- pi- pi+ ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^pi- pi+ ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ pi- pi- ^pi+ ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
                }


Tuple21.ToolList = List_of_Tools

# Additional variables, that do not come by default                                                                                                                                           
Tuple21.addTool(TupleToolDecay, name="Dst")
Tuple21.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h_Dst"]
Tuple21.Dst.addTool(LoKi_All_4h_Dst)

Tuple21.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_4h_Dst"]
Tuple21.Dst.addTool(LoKi_Cone_4h_Dst)

# Additional variables, that do not come by default
Tuple21.addTool(TupleToolDecay, name="D")
Tuple21.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h"]
Tuple21.D.addTool(LoKi_All_4h)

Tuple21.addTool(TupleToolPid,name="TupleToolPid")
Tuple21.TupleToolPid.Verbose = True

Tuple21.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple21.TupleToolGeometry.Verbose = True

Tuple21.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple21.TupleToolTrackInfo.Verbose = True


# TISTOS
#Tuple21.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple21.TupleToolTISTOS.Verbose=True
#Tuple21.TupleToolTISTOS.TriggerList = triggerLines

##Trigger
Tuple21.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.D.addTool(tos)
Tuple21.D.tos.VerboseL0=True
Tuple21.D.tos.VerboseHlt1=True
Tuple21.D.tos.VerboseHlt2=True
Tuple21.D.tos.TriggerList = triggerLines

Tuple21.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.Dst.addTool(tos)
Tuple21.Dst.tos.VerboseL0=True
Tuple21.Dst.tos.VerboseHlt1=True
Tuple21.Dst.tos.VerboseHlt2=True
Tuple21.Dst.tos.TriggerList = triggerLines

Tuple21.addTool(TupleToolDecay, name="h0")
Tuple21.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.h0.addTool(tos)
Tuple21.h0.tos.VerboseL0=True
Tuple21.h0.tos.VerboseHlt1=True
Tuple21.h0.tos.VerboseHlt2=True
Tuple21.h0.tos.TriggerList = triggerLines

Tuple21.addTool(TupleToolDecay, name="h1")
Tuple21.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.h1.addTool(tos)
Tuple21.h1.tos.VerboseL0=True
Tuple21.h1.tos.VerboseHlt1=True
Tuple21.h1.tos.VerboseHlt2=True
Tuple21.h1.tos.TriggerList = triggerLines

Tuple21.addTool(TupleToolDecay, name="mu0")
Tuple21.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.mu0.addTool(tos)
Tuple21.mu0.tos.VerboseL0=True
Tuple21.mu0.tos.VerboseHlt1=True
Tuple21.mu0.tos.VerboseHlt2=True
Tuple21.mu0.tos.TriggerList = triggerLines

Tuple21.addTool(TupleToolDecay, name="mu1")
Tuple21.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple21.mu1.addTool(tos)
Tuple21.mu1.tos.VerboseL0=True
Tuple21.mu1.tos.VerboseHlt1=True
Tuple21.mu1.tos.VerboseHlt2=True
Tuple21.mu1.tos.TriggerList = triggerLines

Tuple21.addTool(TupleToolL0Calo())
Tuple21.TupleToolL0Calo.WhichCalo = "HCAL"
############ Creation of the D*-> D->K+K-Pi+Pi- ntuple #######################
# Def
Tuple22 = DecayTreeTuple("DstD2KKPiPi")
Tuple22.RootInTES      = locationRoot

# Trigger
Tuple22.addTool(tttt)


# Get KKpipi candidates and start the ntuple
#Tuple22.Inputs = [ 'Phys/DstarPromptWithD02HHHHNoPIDLine' ]
Tuple22.Inputs = [ 'Phys/DstarPromptWithD02HHHHLine' ]
Tuple22.Decay =  "[ D*(2010)+ -> ^(D0 -> ^K+ ^K- ^pi+ ^pi- ) ^pi+ ]CC"
Tuple22.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ K- pi+ pi- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> K+ ^K- pi+ pi- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ K- ^pi+ pi- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> K+ K- pi+ ^pi- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> K+ K- pi+ pi- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ K- pi+ pi- ) pi+ ]CC"
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> K+ K- pi+ pi- ) pi+ ]CC"
                }


Tuple22.ToolList = List_of_Tools

# Additional variables, that do not come by default                                                                                                                                          
                                                                                                                                                                                              
Tuple22.addTool(TupleToolDecay, name="Dst")
Tuple22.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h_Dst"]
Tuple22.Dst.addTool(LoKi_All_4h_Dst)

Tuple22.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_4h_Dst"]
Tuple22.Dst.addTool(LoKi_Cone_4h_Dst)

# Additional variables, that do not come by default
Tuple22.addTool(TupleToolDecay, name="D")
Tuple22.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h"]
Tuple22.D.addTool(LoKi_All_4h)

Tuple22.addTool(TupleToolPid,name="TupleToolPid")
Tuple22.TupleToolPid.Verbose = True

Tuple22.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple22.TupleToolGeometry.Verbose = True

Tuple22.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple22.TupleToolTrackInfo.Verbose = True


# TISTOS
#Tuple22.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple22.TupleToolTISTOS.Verbose=True
#Tuple22.TupleToolTISTOS.TriggerList = triggerLines

##Trigger
Tuple22.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.D.addTool(tos)
Tuple22.D.tos.VerboseL0=True
Tuple22.D.tos.VerboseHlt1=True
Tuple22.D.tos.VerboseHlt2=True
Tuple22.D.tos.TriggerList = triggerLines

Tuple22.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.Dst.addTool(tos)
Tuple22.Dst.tos.VerboseL0=True
Tuple22.Dst.tos.VerboseHlt1=True
Tuple22.Dst.tos.VerboseHlt2=True
Tuple22.Dst.tos.TriggerList = triggerLines

Tuple22.addTool(TupleToolDecay, name="h0")
Tuple22.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.h0.addTool(tos)
Tuple22.h0.tos.VerboseL0=True
Tuple22.h0.tos.VerboseHlt1=True
Tuple22.h0.tos.VerboseHlt2=True
Tuple22.h0.tos.TriggerList = triggerLines

Tuple22.addTool(TupleToolDecay, name="h1")
Tuple22.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.h1.addTool(tos)
Tuple22.h1.tos.VerboseL0=True
Tuple22.h1.tos.VerboseHlt1=True
Tuple22.h1.tos.VerboseHlt2=True
Tuple22.h1.tos.TriggerList = triggerLines

Tuple22.addTool(TupleToolDecay, name="mu0")
Tuple22.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.mu0.addTool(tos)
Tuple22.mu0.tos.VerboseL0=True
Tuple22.mu0.tos.VerboseHlt1=True
Tuple22.mu0.tos.VerboseHlt2=True
Tuple22.mu0.tos.TriggerList = triggerLines

Tuple22.addTool(TupleToolDecay, name="mu1")
Tuple22.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple22.mu1.addTool(tos)
Tuple22.mu1.tos.VerboseL0=True
Tuple22.mu1.tos.VerboseHlt1=True
Tuple22.mu1.tos.VerboseHlt2=True
Tuple22.mu1.tos.TriggerList = triggerLines

Tuple22.addTool(TupleToolL0Calo())
Tuple22.TupleToolL0Calo.WhichCalo = "HCAL"
############ Creation of the D*-> D->Pi+Pi-Pi+Pi- ntuple #######################
# Def
Tuple23 = DecayTreeTuple("DstD2PiPiPiPi")
Tuple23.RootInTES      = locationRoot

# Trigger
Tuple23.addTool(tttt)


# Get KKpipi candidates and start the ntuple
#Tuple23.Inputs = [ 'Phys/DstarPromptWithD02HHHHNoPIDLine' ]
Tuple23.Inputs = [ 'Phys/DstarPromptWithD02HHHHLine' ]
Tuple23.Decay =  "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^pi+ ^pi- ) ^pi+ ]CC"
Tuple23.Branches = {
    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- pi+ pi- ) pi+ ]CC"
    ,"h1"         : "[ D*(2010)+ -> (D0 -> pi+ ^pi- pi+ pi- ) pi+ ]CC"
    ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^pi+ pi- ) pi+ ]CC"
    ,"mu1"        : "[ D*(2010)+ -> (D0 -> pi+ pi- pi+ ^pi- ) pi+ ]CC"
    ,"Slowpi"        : "[ D*(2010)+ -> (D0 -> pi+ pi- pi+ pi- ) ^pi+ ]CC"
    ,"D"        : "[ D*(2010)+ -> ^(D0 -> pi+ pi- pi+ pi- ) pi+ ]CC" 
    ,"Dst"          : "[ D*(2010)+ -> (D0 -> pi+ pi- pi+ pi- ) pi+ ]CC"
 }


Tuple23.ToolList = List_of_Tools

# Additional variables, that do not come by default                                                                                                                                          
                                                                                                                                                                                             
Tuple23.addTool(TupleToolDecay, name="Dst")
Tuple23.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h_Dst"]
Tuple23.Dst.addTool(LoKi_All_4h_Dst)

Tuple23.Dst.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Cone_4h_Dst"]
Tuple23.Dst.addTool(LoKi_Cone_4h_Dst)


# Additional variables, that do not come by default
Tuple23.addTool(TupleToolDecay, name="D")
Tuple23.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_All_4h"]
Tuple23.D.addTool(LoKi_All_4h)

Tuple23.addTool(TupleToolPid,name="TupleToolPid")
Tuple23.TupleToolPid.Verbose = True

Tuple23.addTool(TupleToolGeometry,name="TupleToolGeometry")
Tuple23.TupleToolGeometry.Verbose = True

Tuple23.addTool(TupleToolTrackInfo,name="TupleToolTrackInfo")
Tuple23.TupleToolTrackInfo.Verbose = True


# TISTOS
#Tuple23.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#Tuple23.TupleToolTISTOS.Verbose=True
#Tuple23.TupleToolTISTOS.TriggerList = triggerLines

##Trigger
Tuple23.D.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.D.addTool(tos)
Tuple23.D.tos.VerboseL0=True
Tuple23.D.tos.VerboseHlt1=True
Tuple23.D.tos.VerboseHlt2=True
Tuple23.D.tos.TriggerList = triggerLines

Tuple23.Dst.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.Dst.addTool(tos)
Tuple23.Dst.tos.VerboseL0=True
Tuple23.Dst.tos.VerboseHlt1=True
Tuple23.Dst.tos.VerboseHlt2=True
Tuple23.Dst.tos.TriggerList = triggerLines

Tuple23.addTool(TupleToolDecay, name="h0")
Tuple23.h0.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.h0.addTool(tos)
Tuple23.h0.tos.VerboseL0=True
Tuple23.h0.tos.VerboseHlt1=True
Tuple23.h0.tos.VerboseHlt2=True
Tuple23.h0.tos.TriggerList = triggerLines

Tuple23.addTool(TupleToolDecay, name="h1")
Tuple23.h1.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.h1.addTool(tos)
Tuple23.h1.tos.VerboseL0=True
Tuple23.h1.tos.VerboseHlt1=True
Tuple23.h1.tos.VerboseHlt2=True
Tuple23.h1.tos.TriggerList = triggerLines

Tuple23.addTool(TupleToolDecay, name="mu0")
Tuple23.mu0.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.mu0.addTool(tos)
Tuple23.mu0.tos.VerboseL0=True
Tuple23.mu0.tos.VerboseHlt1=True
Tuple23.mu0.tos.VerboseHlt2=True
Tuple23.mu0.tos.TriggerList = triggerLines

Tuple23.addTool(TupleToolDecay, name="mu1")
Tuple23.mu1.ToolList += ["TupleToolTISTOS/tos"]
Tuple23.mu1.addTool(tos)
Tuple23.mu1.tos.VerboseL0=True
Tuple23.mu1.tos.VerboseHlt1=True
Tuple23.mu1.tos.VerboseHlt2=True
Tuple23.mu1.tos.TriggerList = triggerLines

Tuple23.addTool(TupleToolL0Calo())
Tuple23.TupleToolL0Calo.WhichCalo = "HCAL"


Seq_K3Pi_tag = GaudiSequencer("Seq_K3Pi_tag")
Seq_K3Pi_tag.RootInTES = locationRoot
Seq_K3Pi_tag.Members  += [ Tuple20 ]

Seq_K3Pi_WS_tag = GaudiSequencer("Seq_K3Pi_WS_tag")
Seq_K3Pi_WS_tag.RootInTES = locationRoot
Seq_K3Pi_WS_tag.Members  += [ Tuple21 ]

Seq_KKPiPi_tag = GaudiSequencer("Seq_KKPiPi_tag")
Seq_KKPiPi_tag.RootInTES = locationRoot
Seq_KKPiPi_tag.Members  += [ Tuple22 ]

Seq_PiPiPiPi_tag = GaudiSequencer("Seq_PiPiPiPi_tag")
Seq_PiPiPiPi_tag.RootInTES = locationRoot
Seq_PiPiPiPi_tag.Members  += [ Tuple23 ]



# Run only on events in which there's at least a candidate of one of the modes.
#from PhysConf.Filters import LoKi_Filters

#from PhysConf.Filters import LoKi_Filters
#fltrs = LoKi_Filters( STRIP_Code ="""HLT_PASS('StrippingD2XMuMuMicro_2KPiLineDecision')| HLT_PASS('StrippingD2XMuMuMicro_K2PiLineDecision')| HLT_PASS('StrippingD2hhh_PPPLineDecision')| HLT_PASS('StrippingD2XMuMuMicro_KKLineDecision')| HLT_PASS('StrippingD2XMuMuMicro_PiPiLineDecision')| HLT_PASS('StrippingD2XMuMuMicro_K3PiLineDecision')| HLT_PASS('StrippingD2XMuMuMicro_KKPiPiLineDecision')""")



from Configurables import DaVinci

DaVinci().TupleFile = "RARECHARM_4BODIES.ROOT" 

if D_KK_tag:
    DaVinci().UserAlgorithms += [Seq_KK_tag]
if D_PiPi_tag:
    DaVinci().UserAlgorithms += [Seq_PiPi_tag]
if D_KPi_tag:
    DaVinci().UserAlgorithms += [Seq_KPi_tag]
if D_KPi_WS_tag:
    DaVinci().UserAlgorithms += [Seq_KPi_WS_tag]
if D_K3Pi_tag:
    DaVinci().UserAlgorithms += [Seq_K3Pi_tag]
if D_K3Pi_WS_tag:
    DaVinci().UserAlgorithms += [Seq_K3Pi_WS_tag]
if D_KKPiPi_tag:
    DaVinci().UserAlgorithms += [Seq_KKPiPi_tag]
if D_PiPiPiPi_tag:
    DaVinci().UserAlgorithms += [Seq_PiPiPiPi_tag]
